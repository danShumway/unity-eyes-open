﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Eyes" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		
		_blink ("blink", float) = 1
		_stress ("stress", float) = 0.5
	}

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			float _blink;
			float _stress;
			uniform sampler2D _MainTex;

			v2f vert(appdata v) {
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			float4 frag(v2f i) : SV_Target {
				float4 result = tex2D(_MainTex, i.uv);

				//Handle pool recoloring

				//Handle closing eyes
				result.rgb *= 1 - _blink;
				
				//Handle stress tinting
				_stress = _stress * 0.85;
				result = float4(
					result.r + _stress,
					result.g * (1 - _stress),
					result.b * (1 - _stress),
					result.a);

				return result;
			}

			ENDCG
		}
	}
}
