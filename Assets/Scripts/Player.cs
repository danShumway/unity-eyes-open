﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//TODO this should go into a separate class
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	/// <summary> Used for player movement </summary>
	private Rigidbody2D rigidcollider;

	/// <summary> Used for player awareness of monsters </summary>
	private CircleCollider2D vision;

	/// <summary> Current stress level for the player (read-only) </summary>
	public float stress { get; private set; }

	/// <summary> The speed at which you are gaining or losing stress </summary>
	private float stressVelocity = 0;

	/// <summary> Whether or not eyes are open: 0 (open) to 1 (closed) </summary>
	public float blink { get; private set; }

	/// <summary> How long your eyes have been closed </summary>
	private float blinkTime;

	/// <summary> How long until the player can "cancel" stress velocity </summary>
	private float graceTimer;

	/// <summary> How many pools the player character has found </summary>
	public int poolsDiscovered { get; private set; }
	
	/// <summary> What pool the player is standing in (if any) </summary>
	private Water pool;

	/// <summary> Monsters that are visible </summary>
	private List<GameObject> monsters = new List<GameObject>();

	/// <summary> Heartbeat sound - speeds up as you gain stress. </summary>
	private AudioSource heartbeat;

	/// <summary> How much time is remaining before the next heartbeat. </summary>
	private float heartbeatCooldown = 0;
	private float minHeartrate = 60f/50;
	private float maxHeartrate = 60f/140;

	#region CONFIGURATION
	
	/// <summary>
	/// Player movement speed:
	/// 
	/// Keep this on the slow(ish) side. The player shouldn't feel zippy --
	/// You want the awful feeling of being chased in a dream, just not to the point
	/// where it starts getting annoying or distracting.
	/// </summary>
	[SerializeField]
	private float speed = 3.5f;

	[SerializeField]
	private float recoverDelta = 0.01f;

	[SerializeField]
	private float blinkPenalty = 0.02f;

	[SerializeField]
	private float restingStressVelocity = 0.01f;

	[SerializeField]
	private float waterBonus = 0.2f;

	[SerializeField]
	private float waterStressVelocity = -0.01f;

	[SerializeField]
	private float gracePeriod = 1.5f;

	#endregion

	void Start () { this.Init(); }
	public void Init () {
		
		this.stress = 0;
		this.blink = 1;
		this.poolsDiscovered = 0;

		//Collider logic - discover pools, get scared of monsters.
		this.pool = null;
		this.monsters = new List<GameObject>();
		this.rigidcollider = this.GetComponent<Rigidbody2D>();
		this.vision = this.GetComponentInChildren<CircleCollider2D>();
		this.heartbeat = this.GetComponent<AudioSource>(); //TODO: handle this as array.

		CollisionEventer.ClearTriggerEnter2D(this.vision.gameObject);
		CollisionEventer.OnTriggerEnter2D(this.vision.gameObject, collider => {
			GameObject obj = collider.gameObject;
			if (obj.tag == "Enemy") {
				this.monsters.Add(obj);
			}
		});

		CollisionEventer.ClearTriggerExit2D(this.vision.gameObject);
		CollisionEventer.OnTriggerExit2D(this.vision.gameObject, collider => {
			GameObject obj = collider.gameObject;
			if (obj.tag == "Enemy") {
				this.monsters.Remove(obj);
			}
		});

		CollisionEventer.ClearTriggerEnter2D(this.gameObject);
		CollisionEventer.OnTriggerEnter2D(this.gameObject, collider => {
			GameObject parent = collider.transform.parent?.gameObject;
			if (parent?.tag == "Water") {
				this.pool = parent.GetComponent<Water>();

				if (!this.pool.Discovered) {
					this.pool.Discover();
					this.poolsDiscovered++;
				}
			}
		});

		CollisionEventer.ClearTriggerExit2D(this.gameObject);
		CollisionEventer.OnTriggerExit2D(this.gameObject, collider => {
			GameObject parent = collider.transform.parent?.gameObject;
			if (parent?.tag == "Water") {
				this.pool = null;
			}
		});
	}
	
	void Update () {
		this.PlayerInput();
		this.PlayerLogic();
		this.PlayerEffects();
	}

	void PlayerInput () {
		float adjustedSpeed = this.speed;// * Time.deltaTime; -- no longer necessary when using rigidbodies

		var horizontalAxis = Input.GetAxisRaw("Horizontal");
		var verticalAxis = Input.GetAxisRaw("Vertical");

		//Remove controller jitter by giving axes a falloff point
		//This is very minor. I do want the player to still be able to inch around.
		horizontalAxis = horizontalAxis > 0.05f || horizontalAxis < -0.05f ? horizontalAxis : 0;
		verticalAxis = verticalAxis > 0.05f || verticalAxis < -0.05f ? verticalAxis : 0;

		this.rigidcollider.velocity = new Vector2(
			horizontalAxis * adjustedSpeed,
			verticalAxis * adjustedSpeed);

		//TODO: change this to be a transition - use a sine wav to mimic closing eyes.
		bool blinkDown = Input.GetButton("Blink");
		if (blinkDown && this.blink == 0) { //Player has *just* pressed
			this.blinkTime = 0;
		} else if (!blinkDown && this.blink == 1) { //Player has *just* released
			this.blinkTime = 0;
		}

		this.blink = blinkDown ? 1 : 0;
	}
	
	void PlayerLogic () {

		if (this.blink == 0) {

			// Stress velocity does go negative when you're in a pool.
			this.stressVelocity = this.pool == null ?
				this.restingStressVelocity : this.waterStressVelocity;

		} else {

			// The first frame you close your eyes you gain a penalty.
			// This prevents spamming the blink button.
			if (this.blinkTime == 0) {
				this.stressVelocity += this.blinkPenalty;
			}

			// However, if you end up next to a monster that's adding
			// stress at a high enough rate, you'll still die with your eyes closed.
			// To solve this problem, we introduce yet another timer with a grace period.
			// If that timer has run down, you can get your stress velocity immediately
			// reset as soon as you close your eyes. This "ability" takes a set amount
			// of time to recharge again later, and will be less effective until it has recharged.
			if (this.stressVelocity > 0) {
				this.stressVelocity -= this.stressVelocity * (1 - this.graceTimer / this.gracePeriod);
				this.graceTimer = this.gracePeriod;
			}

			this.stressVelocity -= this.recoverDelta * Time.deltaTime;
		}

		if (this.graceTimer > 0) {
			this.graceTimer -= Time.deltaTime;
		}

		// Monsters also increase stress within a given range - only the closest monster takes effect.
		// You also need to have a line of site.
		float closestDistance = Mathf.Infinity;
		GameObject closestMonster = null;
		foreach(GameObject monster in this.monsters) {
			float distance = Vector2.Distance(this.transform.position, monster.transform.position);
			// Vector2 angle = this.transform.position - monster.transform.position;
			// RaycastHit2D[] between = Physics2D.RaycastAll(this.transform.position, angle);

			if (distance < closestDistance) {
				closestMonster = monster;
				closestDistance = distance;
			}
		}

		//If you're not in a pool, and you can see a monster
		if (this.blink == 0 &&
			closestMonster != null &&
			this.pool == null) {

			//TODO Pull this out into a separate set of variables, etc...
			this.stressVelocity += 1 - (closestDistance / 15);
		}

		//If you are *practically* touching a monster, closing your eyes won't help you.
		//I could use an actual separate collision with the monster body for this, but there's no need.
		//This collision is only ever calculated when eyes are closed
		//so for all practical purposes, the player will never notice that it's
		//calculated based on distance.
		if (closestMonster != null &&
			closestDistance < 1.8f &&
			this.pool == null) {
				
			this.stressVelocity = 0.9f;
		}

		//Bound stress velocity to prevent overflows in either direction.
		//Being in water allows you to temporarily drop below zero stress.
		float minStress = this.pool == null ? 0 : 0 - this.waterBonus;
		float maxStress = 1;

		float stressDelta = this.stressVelocity * Time.deltaTime;
		stressDelta = Mathf.Max(
			Mathf.Min(stressDelta, maxStress - this.stress),
			Mathf.Min(minStress - this.stress, 0));

		this.stress += stressDelta;
		this.blinkTime += Time.deltaTime;

		//Level transitions
		//The player activates the pool by closing their eyes inside of it while at min stress.
		if (this.pool != null && 
			this.stress == minStress &&
			this.blink == 1) {
				
			this.pool.Activate();
		}
	}

	void PlayerEffects () {
		float stressAdjustedHeartbeat = this.minHeartrate - this.stress * (this.minHeartrate - this.maxHeartrate);
		this.heartbeatCooldown = Mathf.Min(this.heartbeatCooldown, stressAdjustedHeartbeat);
		this.heartbeatCooldown -= Time.deltaTime;

		if (this.heartbeatCooldown < 0) {
			this.heartbeat.volume = this.stress;
			this.heartbeat.Play();

			this.heartbeatCooldown = this.minHeartrate;
		}

		//controller rumble on heartbeat. Not entirely sure how I'm going to do that.
		//UnityEngine.Handheld.Vibrate();
	}
}
