﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Monster : MonoBehaviour {

	#region CONFIG

	/// <summary>
	/// Maximum amount of time a monster will spend
	/// inside of a "normal" state.
	/// </summary>
	[SerializeField]
	private float maxStateQueue = 3;

	/// <summary>
	/// Minimum amount of time a monster will spend
	/// inside of a "normal" state.
	/// </summary>
	[SerializeField]
	private float minStateQueue = 1;

	/// <summary>
	/// How many degrees wide a monster's cone of vision is.
	/// Note that this is applied in both directions
	/// so if you wanted a monster to see 180 degrees
	/// you would set this value to 90. 
	/// </summary>
	[SerializeField]
	private float fov = 40;

	#endregion

	/// <summary> Used for movement </summary>
	private Rigidbody2D rigidcollider;

	/// <summary> Used for awareness of player/other entities </summary>
	private CircleCollider2D vision;

	/// <summary> List of gameobjects the creature is aware of </summary>
	private List<GameObject> aware = new List<GameObject>();

	/// <summary>
	/// Monster state is pretty simple.
	///
	/// Each state has a very small routine and a set of triggers
	/// that will set the state to something else.
	/// Monster AI should be very simple and abstract.
	/// </summary>
	private MonsterState state;
	private enum MonsterState {
		Wait, Rotate, Move
	}

	/// <summary> How long (in seconds) a monster should stay in the current state </summary>
	private float stateRemaining;

	/// <summary>Whether to rotate forward (true) or backwards (false)</summary>
	private bool rotationDirection;

	/// <summary>Target that the monster is charging at.</summary>
	private GameObject chargeTarget;

	/// <summary>Played when charging.</summary>
	private AudioSource howl;

	void Start () { this.Init(); }
	public void Init () {
		this.rigidcollider = this.GetComponent<Rigidbody2D>();
		this.vision = this.GetComponentInChildren<CircleCollider2D>();
		this.howl = this.GetComponent<AudioSource>();

		CollisionEventer.OnTriggerEnter2D(this.vision.gameObject, collider => {
			GameObject obj = collider.gameObject;
			this.aware.Add(obj);
		});

		CollisionEventer.OnTriggerExit2D(this.vision.gameObject, collider => {
			GameObject obj = collider.gameObject;
			this.aware.Remove(obj);
		});
	}

	private bool facing (GameObject obj) {
		Vector3 difference = obj.transform.position - this.transform.position;
		float rotation = Mathf.Atan2(difference.y, difference.x) * 180 / Mathf.PI;
		float facing = this.transform.rotation.eulerAngles.z;

		//Normalize
		rotation = rotation % 360;
		if (rotation < 0) { rotation += 360; }

		if (360 - Mathf.Abs(rotation - facing) < this.fov ||
			Mathf.Abs(rotation - facing) < this.fov) {

		    //Run raycast last for performance reasons
			RaycastHit2D[] between = Physics2D.RaycastAll(this.transform.position, difference);
			Array.Sort(between, (ob1, ob2) => {
				return ob1.distance > ob2.distance ? 1 : 0;
			});

			foreach (RaycastHit2D obstical in between) {
				if (obstical.transform.gameObject.tag == "wall" ||
					obstical.transform.gameObject.tag == "Water") {

					return false;
				}

				if (obstical.transform.gameObject == obj) {
					return true;
				}
			}
		}

		return false;
	}
	
	void Update () {
		//Reset everything. Monsters have basically zero physics.
		this.rigidcollider.velocity = new Vector2(0, 0);
		this.rigidcollider.angularVelocity = 0;

		//Start by running vision calculations to determine if we should charge.
		GameObject newTarget = null;
		foreach(GameObject obj in this.aware) {
			if (obj.tag == "Player" && this.facing(obj)) { //TODO: check if player is actually visible
				newTarget = obj;
			}
		}

		//Rotate towards the target on the *first* frame.
		//This makes monsters more dangerous, but allows you to still dodge them. 
		if (newTarget != null && this.chargeTarget == null) {

			Vector3 difference = newTarget.transform.position - this.transform.position;
			float rotation = Mathf.Atan2(difference.y, difference.x) * 180 / Mathf.PI;
			Quaternion targetQuat = Quaternion.Euler(0, 0, rotation);
			this.rigidcollider.transform.rotation = targetQuat;

			//Also the first time a monster sees you, howl.
			//For the same reasons. You need an indication.
			//And it's all spoopy, 'cause it is October.
			this.howl.Play();
		}

		this.chargeTarget = newTarget;

		if (this.chargeTarget) {
			Vector2 speed = new Vector2(15, 0);
			speed = this.transform.rotation * speed;
			this.rigidcollider.velocity = speed;
			return;
		}

		switch (this.state) {
			case MonsterState.Move:
				Vector2 speed = new Vector2(3, 0);
				speed = this.transform.rotation * speed;
				this.rigidcollider.velocity = speed;
			break;

			case MonsterState.Rotate:
			
				float rotationVelocity = 36 * (this.rotationDirection ? 1 : -1);
				float targetRotation = 
					this.rigidcollider.transform.rotation.eulerAngles.z + rotationVelocity * Time.deltaTime;
				
				Quaternion target = Quaternion.Euler(0, 0, targetRotation);
				this.rigidcollider.transform.rotation = target;
			break;
		}

		this.stateRemaining -= Time.deltaTime;
		if (this.stateRemaining <= 0) {
			Array values = Enum.GetValues(typeof(MonsterState));
			int next = UnityEngine.Random.Range(0, values.Length);
			this.state = (MonsterState)values.GetValue(next);

			this.stateRemaining = UnityEngine.Random.Range(this.minStateQueue, this.maxStateQueue);

			//Specifically for rotation, we need to pick a direction.
			//Yes, it's a little annoying.
			this.rotationDirection = Mathf.Round(
				UnityEngine.Random.Range(0, 1)) == 1 ? true : false;
		}
	}
}
