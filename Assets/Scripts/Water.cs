﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour {

	private GameObject player;
	private GameObject collision;
	private GameObject trigger;

	public delegate void Activator ();
	private Activator activator;
	public bool Active { get; private set; }
	public bool Discovered { get; private set; }

	void Awake () {
		this.Discovered = true; //By default, a pool is pre-discovered
	}

	void Start () {

		this.player = GameObject.Find("Player");
		this.collision = this.transform.Find("Collider").gameObject;
		this.trigger = this.transform.Find("Trigger").gameObject;
		
		//Always disable collision for player.
		Physics2D.IgnoreCollision(
			this.collision.GetComponent<BoxCollider2D>(),
			player.GetComponent<BoxCollider2D>());
	}

	public void SetActivator (Activator activator) {
		this.activator = activator;
		this.Active = true;
		this.Discovered = false; //Active pools are undiscovered
	}

	public void Discover () {
		this.Discovered = true;
	}

	public void Activate () {
		if (this.activator == null) { return; }
		this.activator.Invoke();
	}
}
