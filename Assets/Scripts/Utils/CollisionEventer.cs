﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEventer : MonoBehaviour {

	public delegate void TriggerListener(Collider2D other);
	private List<TriggerListener> enterListeners = new List<TriggerListener>();
	private List<TriggerListener> exitListeners = new List<TriggerListener>();

	void Start () {}
	void OnTriggerEnter2D (Collider2D other) {
		foreach (TriggerListener listener in this.enterListeners) {
			listener.Invoke(other);
		}
	}

	void OnTriggerExit2D (Collider2D other) {
		foreach (TriggerListener listener in this.exitListeners) {
			listener.Invoke(other);
		}
	}

	// Overrides to support attach
	public void OnTriggerEnter2D (TriggerListener listener) {
		this.enterListeners.Add(listener);
	}

	public void OnTriggerExit2D (TriggerListener listener) {
		this.exitListeners.Add(listener);
	}

	public void ClearTriggerEnter2D () {
		this.enterListeners.Clear();
	}

	public void ClearTriggerExit2D () {
		this.exitListeners.Clear();
	}

	public static void OnTriggerEnter2D (GameObject collider, TriggerListener listener) {
		
		CollisionEventer eventer = 
			collider.GetComponent<CollisionEventer>() ??
			collider.AddComponent<CollisionEventer>();

		eventer.OnTriggerEnter2D(listener);
	}

	public static void OnTriggerExit2D (GameObject collider, TriggerListener listener) {
		
		CollisionEventer eventer = 
			collider.GetComponent<CollisionEventer>() ??
			collider.AddComponent<CollisionEventer>();

		eventer.OnTriggerExit2D(listener);
	}

	public static void ClearTriggerEnter2D (GameObject collider) {
		
		CollisionEventer eventer = 
			collider.GetComponent<CollisionEventer>() ??
			collider.AddComponent<CollisionEventer>();

		eventer.ClearTriggerEnter2D();
	}

	
	public static void ClearTriggerExit2D (GameObject collider) {
		
		CollisionEventer eventer = 
			collider.GetComponent<CollisionEventer>() ??
			collider.AddComponent<CollisionEventer>();

		eventer.ClearTriggerExit2D();
	}
}
