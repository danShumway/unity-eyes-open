﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamera : MonoBehaviour {

	[SerializeField]
	private GameObject menu;

	[SerializeField]
	private Material menuMaterial;

	void OnRenderImage(RenderTexture source, RenderTexture destination) {
		MainMenu menu = this.menu.GetComponent<MainMenu>();

		//Linear fade once half of the blink time has expired.
		float fadeStart = menu.menuDuration / 2;
		float fade = Mathf.Max(menu.blinkTime - fadeStart, 0) / (menu.menuDuration - fadeStart);
		
		this.menuMaterial.SetFloat("_fade", fade);
		Graphics.Blit(source, destination, this.menuMaterial);
	}
}
