﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PlayerCamera : MonoBehaviour {

	private Player player;

	[SerializeField]
	private Material cameraMaterial;

	void Start () {
		player = GameObject.Find("Player").GetComponent<Player>();
	}
	
	void Update () {
		//TODO: Anything more fancy than just following the player?
		Vector3 playerPosition = this.player.transform.position;
		double correctedX = Mathf.Round(playerPosition.x * 127) / 127;
		double correctedY = Mathf.Round(playerPosition.y * 127) / 127;
		this.transform.position = new Vector3(
			(float)correctedX,
			(float)correctedY,
			this.transform.position.z
		 );
	}

	void OnRenderImage(RenderTexture source, RenderTexture destination) {

		this.cameraMaterial.SetFloat("_blink", this.player.blink);
		this.cameraMaterial.SetFloat("_stress", this.player.stress);

		Graphics.Blit(source, destination, this.cameraMaterial);
	}
}
