﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : MonoBehaviour {

	[SerializeField]
	private GameObject player;
	private Player playerComponent;

	[SerializeField]
	private GameObject deathScreen;

	[SerializeField]
	private GameObject level;
	private Level levelComponent;

	private Text score;

	void Start () { this.Init(); }
	public void Init () {
		this.score = GameObject.Find("GameScore").GetComponent<Text>();
		this.levelComponent = this.level.GetComponent<Level>();
		this.levelComponent.Init();

		this.playerComponent = this.player.GetComponent<Player>();
		this.playerComponent.Init();
	}
	
	void Update () {
		
		this.score.text = this.playerComponent.poolsDiscovered.ToString();

		if (this.playerComponent.stress >= 1) {
			this.levelComponent.Deactivate();
			this.gameObject.SetActive(false);
			this.deathScreen.SetActive(true);
			
			this.deathScreen.GetComponent<DeathMenu>().Init();
		}
	}
}
