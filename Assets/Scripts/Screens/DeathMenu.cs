﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathMenu : MonoBehaviour {

	public float menuDuration { get; private set; }

	[SerializeField]
	private GameObject main;

	[SerializeField]
	private GameObject player;
	private Player playerComponent;

	private Text score;
	public float time { get; private set; }

	void Start () { this.Init(); }
	public void Init() {
		this.score = GameObject.Find("Score").GetComponent<Text>();
		this.playerComponent = this.player.GetComponent<Player>();

		this.menuDuration = 4;
		this.time = 0;

		this.score.text = "You discovered " +
			this.playerComponent.poolsDiscovered +
			" pools.";
	}

	void Update () {

		//Death Menu doesn't require you to close your eyes.
		//It just expires on a timer.
		this.time += Time.deltaTime;

		if (this.time > this.menuDuration) {
			this.gameObject.SetActive(false);
			this.main.SetActive(true);

			this.main.GetComponent<MainMenu>().Init();
		}
	}
}
