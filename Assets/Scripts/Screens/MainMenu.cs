﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public float menuDuration { get; private set; }

	[SerializeField]
	private GameObject game;

	public float blinkTime { get; private set; }

	void Start () { this.Init(); }
	public void Init () {
		this.menuDuration = 2;
		this.blinkTime = 0;

		//Reset any static variables.
		//Should these just be stored on the game?
		//Probably
	}
	
	void Update () {
		
		bool blinkDown = Input.GetButton("Blink");
		if (blinkDown) {
			this.blinkTime += Time.deltaTime;
		} else {
			this.blinkTime = 0;
		}

		if (this.blinkTime > this.menuDuration) {

			this.gameObject.SetActive(false);
			this.game.SetActive(true);

			this.game.GetComponent<GameScreen>().Init();
		}
	}
}
