﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CollisionEventer
struct CollisionEventer_t1111808458;
// CollisionEventer/TriggerListener
struct TriggerListener_t1115224763;
// CollisionEventer/TriggerListener[]
struct TriggerListenerU5BU5D_t3133592570;
// DeathMenu
struct DeathMenu_t1753622066;
// GameScreen
struct GameScreen_t1800529819;
// Level
struct Level_t2237665516;
// Level/TileType[0...,0...]
struct TileTypeU5B0___U2C0___U5D_t2292257856;
// MainMenu
struct MainMenu_t3798339593;
// MenuCamera
struct MenuCamera_t4282292518;
// Monster
struct Monster_t1049719775;
// Player
struct Player_t3266647312;
// PlayerCamera
struct PlayerCamera_t1622178205;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1286104214;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<CollisionEventer/TriggerListener>
struct List_1_t2587299505;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Comparison`1<UnityEngine.RaycastHit2D>
struct Comparison_1_t2054513168;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Void
struct Void_t1185182177;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.AudioSourceExtension
struct AudioSourceExtension_t3064908834;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t3581341831;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_t662546754;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Tilemaps.Tile
struct Tile_t1378929773;
// UnityEngine.Tilemaps.TileBase
struct TileBase_t3985733243;
// UnityEngine.Tilemaps.Tilemap
struct Tilemap_t1578933799;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// Water
struct Water_t1083516957;
// Water/Activator
struct Activator_t2573376075;

extern RuntimeClass* Action_1_t1286104214_il2cpp_TypeInfo_var;
extern RuntimeClass* Activator_t2573376075_il2cpp_TypeInfo_var;
extern RuntimeClass* Comparison_1_t2054513168_il2cpp_TypeInfo_var;
extern RuntimeClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphics_t783367614_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* Level_t2237665516_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2585711361_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2587299505_il2cpp_TypeInfo_var;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* MonsterState_t2135005343_il2cpp_TypeInfo_var;
extern RuntimeClass* Monster_t1049719775_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern RuntimeClass* TileTypeU5B0___U2C0___U5D_t2292257856_il2cpp_TypeInfo_var;
extern RuntimeClass* TriggerListener_t1115224763_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2Int_t3469998543_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1031853882;
extern String_t* _stringLiteral1189462381;
extern String_t* _stringLiteral1201842151;
extern String_t* _stringLiteral1512031223;
extern String_t* _stringLiteral1652603074;
extern String_t* _stringLiteral1769055757;
extern String_t* _stringLiteral1828639942;
extern String_t* _stringLiteral2261822918;
extern String_t* _stringLiteral2359378951;
extern String_t* _stringLiteral2604872252;
extern String_t* _stringLiteral27031491;
extern String_t* _stringLiteral2720163479;
extern String_t* _stringLiteral2984908384;
extern String_t* _stringLiteral3191826632;
extern String_t* _stringLiteral3513529601;
extern String_t* _stringLiteral3522276777;
extern String_t* _stringLiteral3587759650;
extern String_t* _stringLiteral3701375849;
extern String_t* _stringLiteral39716438;
extern String_t* _stringLiteral4086148775;
extern String_t* _stringLiteral760905195;
extern String_t* _stringLiteral78692563;
extern String_t* _stringLiteral7955884;
extern const RuntimeMethod* Action_1__ctor_m2622589598_RuntimeMethod_var;
extern const RuntimeMethod* Array_Sort_TisRaycastHit2D_t2279581989_m3365424645_RuntimeMethod_var;
extern const RuntimeMethod* Comparison_1__ctor_m1180004629_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1341201278_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m765708873_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1996258908_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m4286844348_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m4179928398_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m959559447_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisDeathMenu_t1753622066_m3748280843_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisGameScreen_t1800529819_m2973029269_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisLevel_t2237665516_m1206308685_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisPlayer_t3266647312_m4068145281_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisTilemap_t1578933799_m3651117485_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisWater_t1083516957_m400380985_RuntimeMethod_var;
extern const RuntimeMethod* Level_U3CDeactivateU3Em__0_m4082416521_RuntimeMethod_var;
extern const RuntimeMethod* Level_U3CGenerateU3Em__1_m2845411788_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1077912546_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m2765963565_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m3309761785_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ForEach_m2842337338_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m1750140655_RuntimeMethod_var;
extern const RuntimeMethod* List_1_GetEnumerator_m3837348380_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m4063777476_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1424466557_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3280379170_RuntimeMethod_var;
extern const RuntimeMethod* Monster_U3CInitU3Em__0_m4099899884_RuntimeMethod_var;
extern const RuntimeMethod* Monster_U3CInitU3Em__1_m3061626409_RuntimeMethod_var;
extern const RuntimeMethod* Monster_U3CfacingU3Em__2_m166827732_RuntimeMethod_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var;
extern const RuntimeMethod* Player_U3CInitU3Em__0_m3753943790_RuntimeMethod_var;
extern const RuntimeMethod* Player_U3CInitU3Em__1_m102196894_RuntimeMethod_var;
extern const RuntimeMethod* Player_U3CInitU3Em__2_m1137546408_RuntimeMethod_var;
extern const RuntimeMethod* Player_U3CInitU3Em__3_m2803041481_RuntimeMethod_var;
extern const RuntimeType* MonsterState_t2135005343_0_0_0_var;
extern const uint32_t CollisionEventer_ClearTriggerEnter2D_m448421859_MetadataUsageId;
extern const uint32_t CollisionEventer_ClearTriggerEnter2D_m671181426_MetadataUsageId;
extern const uint32_t CollisionEventer_ClearTriggerExit2D_m42893453_MetadataUsageId;
extern const uint32_t CollisionEventer_ClearTriggerExit2D_m704284422_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerEnter2D_m1653861538_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerEnter2D_m1969312576_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerEnter2D_m3032026089_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerExit2D_m3144928147_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerExit2D_m3161418375_MetadataUsageId;
extern const uint32_t CollisionEventer_OnTriggerExit2D_m61367443_MetadataUsageId;
extern const uint32_t CollisionEventer__ctor_m2507749900_MetadataUsageId;
extern const uint32_t DeathMenu_Init_m3830644225_MetadataUsageId;
extern const uint32_t DeathMenu_Update_m993778007_MetadataUsageId;
extern const uint32_t GameScreen_Init_m2409608521_MetadataUsageId;
extern const uint32_t GameScreen_Update_m3339885975_MetadataUsageId;
extern const uint32_t Level_Deactivate_m271886484_MetadataUsageId;
extern const uint32_t Level_Generate_m932420581_MetadataUsageId;
extern const uint32_t Level_Init_m3675134387_MetadataUsageId;
extern const uint32_t Level_U3CDeactivateU3Em__0_m4082416521_MetadataUsageId;
extern const uint32_t Level__ctor_m38370482_MetadataUsageId;
extern const uint32_t Level_cutChannel_m3357319375_MetadataUsageId;
extern const uint32_t Level_instantiate_m2506382120_MetadataUsageId;
extern const uint32_t MainMenu_Update_m1422991247_MetadataUsageId;
extern const uint32_t MenuCamera_OnRenderImage_m2799149731_MetadataUsageId;
extern const uint32_t Monster_Init_m664643989_MetadataUsageId;
extern const uint32_t Monster_U3CInitU3Em__0_m4099899884_MetadataUsageId;
extern const uint32_t Monster_U3CInitU3Em__1_m3061626409_MetadataUsageId;
extern const uint32_t Monster_Update_m1264681689_MetadataUsageId;
extern const uint32_t Monster__ctor_m2987639624_MetadataUsageId;
extern const uint32_t Monster_facing_m1121058899_MetadataUsageId;
extern const uint32_t PlayerCamera_OnRenderImage_m341268003_MetadataUsageId;
extern const uint32_t PlayerCamera_Start_m3648269509_MetadataUsageId;
extern const uint32_t PlayerCamera_Update_m1529155267_MetadataUsageId;
extern const uint32_t Player_Init_m3348666991_MetadataUsageId;
extern const uint32_t Player_PlayerEffects_m1429873540_MetadataUsageId;
extern const uint32_t Player_PlayerInput_m1067569136_MetadataUsageId;
extern const uint32_t Player_PlayerLogic_m3769131123_MetadataUsageId;
extern const uint32_t Player_U3CInitU3Em__0_m3753943790_MetadataUsageId;
extern const uint32_t Player_U3CInitU3Em__1_m102196894_MetadataUsageId;
extern const uint32_t Player_U3CInitU3Em__2_m1137546408_MetadataUsageId;
extern const uint32_t Player_U3CInitU3Em__3_m2803041481_MetadataUsageId;
extern const uint32_t Player__ctor_m509448900_MetadataUsageId;
extern const uint32_t Water_Start_m3523044285_MetadataUsageId;

struct TileTypeU5B0___U2C0___U5D_t2292257856;
struct DelegateU5BU5D_t1703627840;
struct RaycastHit2DU5BU5D_t4286651560;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T2587299505_H
#define LIST_1_T2587299505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<CollisionEventer/TriggerListener>
struct  List_1_t2587299505  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TriggerListenerU5BU5D_t3133592570* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2587299505, ____items_1)); }
	inline TriggerListenerU5BU5D_t3133592570* get__items_1() const { return ____items_1; }
	inline TriggerListenerU5BU5D_t3133592570** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TriggerListenerU5BU5D_t3133592570* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2587299505, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2587299505, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2587299505, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2587299505_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TriggerListenerU5BU5D_t3133592570* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2587299505_StaticFields, ____emptyArray_5)); }
	inline TriggerListenerU5BU5D_t3133592570* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TriggerListenerU5BU5D_t3133592570** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TriggerListenerU5BU5D_t3133592570* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2587299505_H
#ifndef LIST_1_T2585711361_H
#define LIST_1_T2585711361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct  List_1_t2585711361  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_t3328599146* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____items_1)); }
	inline GameObjectU5BU5D_t3328599146* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_t3328599146* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2585711361, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2585711361_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_t3328599146* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2585711361_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_t3328599146* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_t3328599146* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2585711361_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUMERATOR_T181576086_H
#define ENUMERATOR_T181576086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<CollisionEventer/TriggerListener>
struct  Enumerator_t181576086 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2587299505 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	TriggerListener_t1115224763 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t181576086, ___list_0)); }
	inline List_1_t2587299505 * get_list_0() const { return ___list_0; }
	inline List_1_t2587299505 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2587299505 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t181576086, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t181576086, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t181576086, ___current_3)); }
	inline TriggerListener_t1115224763 * get_current_3() const { return ___current_3; }
	inline TriggerListener_t1115224763 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(TriggerListener_t1115224763 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T181576086_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t257213610 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___list_0)); }
	inline List_1_t257213610 * get_list_0() const { return ___list_0; }
	inline List_1_t257213610 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t257213610 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef ENUMERATOR_T179987942_H
#define ENUMERATOR_T179987942_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct  Enumerator_t179987942 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t2585711361 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_t1113636619 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___list_0)); }
	inline List_1_t2585711361 * get_list_0() const { return ___list_0; }
	inline List_1_t2585711361 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t2585711361 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t179987942, ___current_3)); }
	inline GameObject_t1113636619 * get_current_3() const { return ___current_3; }
	inline GameObject_t1113636619 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_t1113636619 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T179987942_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR2INT_T3469998543_H
#define VECTOR2INT_T3469998543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t3469998543 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t3469998543_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t3469998543  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t3469998543  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t3469998543  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t3469998543  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t3469998543  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t3469998543  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t3469998543  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t3469998543 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t3469998543  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_One_3)); }
	inline Vector2Int_t3469998543  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t3469998543 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t3469998543  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t3469998543  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t3469998543 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t3469998543  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t3469998543  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t3469998543 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t3469998543  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t3469998543  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t3469998543 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t3469998543  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t3469998543  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t3469998543 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t3469998543  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T3469998543_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR3INT_T741115188_H
#define VECTOR3INT_T741115188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3Int
struct  Vector3Int_t741115188 
{
public:
	// System.Int32 UnityEngine.Vector3Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector3Int::m_Y
	int32_t ___m_Y_1;
	// System.Int32 UnityEngine.Vector3Int::m_Z
	int32_t ___m_Z_2;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}

	inline static int32_t get_offset_of_m_Z_2() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188, ___m_Z_2)); }
	inline int32_t get_m_Z_2() const { return ___m_Z_2; }
	inline int32_t* get_address_of_m_Z_2() { return &___m_Z_2; }
	inline void set_m_Z_2(int32_t value)
	{
		___m_Z_2 = value;
	}
};

struct Vector3Int_t741115188_StaticFields
{
public:
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Zero
	Vector3Int_t741115188  ___s_Zero_3;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_One
	Vector3Int_t741115188  ___s_One_4;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Up
	Vector3Int_t741115188  ___s_Up_5;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Down
	Vector3Int_t741115188  ___s_Down_6;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Left
	Vector3Int_t741115188  ___s_Left_7;
	// UnityEngine.Vector3Int UnityEngine.Vector3Int::s_Right
	Vector3Int_t741115188  ___s_Right_8;

public:
	inline static int32_t get_offset_of_s_Zero_3() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_Zero_3)); }
	inline Vector3Int_t741115188  get_s_Zero_3() const { return ___s_Zero_3; }
	inline Vector3Int_t741115188 * get_address_of_s_Zero_3() { return &___s_Zero_3; }
	inline void set_s_Zero_3(Vector3Int_t741115188  value)
	{
		___s_Zero_3 = value;
	}

	inline static int32_t get_offset_of_s_One_4() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_One_4)); }
	inline Vector3Int_t741115188  get_s_One_4() const { return ___s_One_4; }
	inline Vector3Int_t741115188 * get_address_of_s_One_4() { return &___s_One_4; }
	inline void set_s_One_4(Vector3Int_t741115188  value)
	{
		___s_One_4 = value;
	}

	inline static int32_t get_offset_of_s_Up_5() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_Up_5)); }
	inline Vector3Int_t741115188  get_s_Up_5() const { return ___s_Up_5; }
	inline Vector3Int_t741115188 * get_address_of_s_Up_5() { return &___s_Up_5; }
	inline void set_s_Up_5(Vector3Int_t741115188  value)
	{
		___s_Up_5 = value;
	}

	inline static int32_t get_offset_of_s_Down_6() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_Down_6)); }
	inline Vector3Int_t741115188  get_s_Down_6() const { return ___s_Down_6; }
	inline Vector3Int_t741115188 * get_address_of_s_Down_6() { return &___s_Down_6; }
	inline void set_s_Down_6(Vector3Int_t741115188  value)
	{
		___s_Down_6 = value;
	}

	inline static int32_t get_offset_of_s_Left_7() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_Left_7)); }
	inline Vector3Int_t741115188  get_s_Left_7() const { return ___s_Left_7; }
	inline Vector3Int_t741115188 * get_address_of_s_Left_7() { return &___s_Left_7; }
	inline void set_s_Left_7(Vector3Int_t741115188  value)
	{
		___s_Left_7 = value;
	}

	inline static int32_t get_offset_of_s_Right_8() { return static_cast<int32_t>(offsetof(Vector3Int_t741115188_StaticFields, ___s_Right_8)); }
	inline Vector3Int_t741115188  get_s_Right_8() const { return ___s_Right_8; }
	inline Vector3Int_t741115188 * get_address_of_s_Right_8() { return &___s_Right_8; }
	inline void set_s_Right_8(Vector3Int_t741115188  value)
	{
		___s_Right_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3INT_T741115188_H
#ifndef TILETYPE_T759873050_H
#define TILETYPE_T759873050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level/TileType
struct  TileType_t759873050 
{
public:
	// System.Int32 Level/TileType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TileType_t759873050, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILETYPE_T759873050_H
#ifndef MONSTERSTATE_T2135005343_H
#define MONSTERSTATE_T2135005343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Monster/MonsterState
struct  MonsterState_t2135005343 
{
public:
	// System.Int32 Monster/MonsterState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MonsterState_t2135005343, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONSTERSTATE_T2135005343_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef COLLIDERTYPE_T2056028780_H
#define COLLIDERTYPE_T2056028780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Tilemaps.Tile/ColliderType
struct  ColliderType_t2056028780 
{
public:
	// System.Int32 UnityEngine.Tilemaps.Tile/ColliderType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColliderType_t2056028780, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDERTYPE_T2056028780_H
#ifndef TILEFLAGS_T2429973456_H
#define TILEFLAGS_T2429973456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Tilemaps.TileFlags
struct  TileFlags_t2429973456 
{
public:
	// System.Int32 UnityEngine.Tilemaps.TileFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TileFlags_t2429973456, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEFLAGS_T2429973456_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t426314064 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t426314064 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2999457153 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2999457153 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef TRIGGERLISTENER_T1115224763_H
#define TRIGGERLISTENER_T1115224763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionEventer/TriggerListener
struct  TriggerListener_t1115224763  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTENER_T1115224763_H
#ifndef ACTION_1_T1286104214_H
#define ACTION_1_T1286104214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.GameObject>
struct  Action_1_t1286104214  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1286104214_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
#ifndef COMPARISON_1_T2054513168_H
#define COMPARISON_1_T2054513168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Comparison`1<UnityEngine.RaycastHit2D>
struct  Comparison_1_t2054513168  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPARISON_1_T2054513168_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef RENDERTEXTURE_T2108887433_H
#define RENDERTEXTURE_T2108887433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2108887433  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2108887433_H
#ifndef RIGIDBODY2D_T939494601_H
#define RIGIDBODY2D_T939494601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rigidbody2D
struct  Rigidbody2D_t939494601  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RIGIDBODY2D_T939494601_H
#ifndef TILEBASE_T3985733243_H
#define TILEBASE_T3985733243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Tilemaps.TileBase
struct  TileBase_t3985733243  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEBASE_T3985733243_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:
	// System.Int32 UnityEngine.Transform::<hierarchyCount>k__BackingField
	int32_t ___U3ChierarchyCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ChierarchyCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Transform_t3600365921, ___U3ChierarchyCountU3Ek__BackingField_4)); }
	inline int32_t get_U3ChierarchyCountU3Ek__BackingField_4() const { return ___U3ChierarchyCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3ChierarchyCountU3Ek__BackingField_4() { return &___U3ChierarchyCountU3Ek__BackingField_4; }
	inline void set_U3ChierarchyCountU3Ek__BackingField_4(int32_t value)
	{
		___U3ChierarchyCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ACTIVATOR_T2573376075_H
#define ACTIVATOR_T2573376075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water/Activator
struct  Activator_t2573376075  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATOR_T2573376075_H
#ifndef AUDIOBEHAVIOUR_T2879336574_H
#define AUDIOBEHAVIOUR_T2879336574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioBehaviour
struct  AudioBehaviour_t2879336574  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOBEHAVIOUR_T2879336574_H
#ifndef COLLIDER2D_T2806799626_H
#define COLLIDER2D_T2806799626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider2D
struct  Collider2D_t2806799626  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER2D_T2806799626_H
#ifndef GRIDLAYOUT_T754322041_H
#define GRIDLAYOUT_T754322041_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GridLayout
struct  GridLayout_t754322041  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUT_T754322041_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef TILE_T1378929773_H
#define TILE_T1378929773_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Tilemaps.Tile
struct  Tile_t1378929773  : public TileBase_t3985733243
{
public:
	// UnityEngine.Sprite UnityEngine.Tilemaps.Tile::m_Sprite
	Sprite_t280657092 * ___m_Sprite_4;
	// UnityEngine.Color UnityEngine.Tilemaps.Tile::m_Color
	Color_t2555686324  ___m_Color_5;
	// UnityEngine.Matrix4x4 UnityEngine.Tilemaps.Tile::m_Transform
	Matrix4x4_t1817901843  ___m_Transform_6;
	// UnityEngine.GameObject UnityEngine.Tilemaps.Tile::m_InstancedGameObject
	GameObject_t1113636619 * ___m_InstancedGameObject_7;
	// UnityEngine.Tilemaps.TileFlags UnityEngine.Tilemaps.Tile::m_Flags
	int32_t ___m_Flags_8;
	// UnityEngine.Tilemaps.Tile/ColliderType UnityEngine.Tilemaps.Tile::m_ColliderType
	int32_t ___m_ColliderType_9;

public:
	inline static int32_t get_offset_of_m_Sprite_4() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_Sprite_4)); }
	inline Sprite_t280657092 * get_m_Sprite_4() const { return ___m_Sprite_4; }
	inline Sprite_t280657092 ** get_address_of_m_Sprite_4() { return &___m_Sprite_4; }
	inline void set_m_Sprite_4(Sprite_t280657092 * value)
	{
		___m_Sprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Sprite_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_Color_5)); }
	inline Color_t2555686324  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2555686324 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2555686324  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_Transform_6)); }
	inline Matrix4x4_t1817901843  get_m_Transform_6() const { return ___m_Transform_6; }
	inline Matrix4x4_t1817901843 * get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Matrix4x4_t1817901843  value)
	{
		___m_Transform_6 = value;
	}

	inline static int32_t get_offset_of_m_InstancedGameObject_7() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_InstancedGameObject_7)); }
	inline GameObject_t1113636619 * get_m_InstancedGameObject_7() const { return ___m_InstancedGameObject_7; }
	inline GameObject_t1113636619 ** get_address_of_m_InstancedGameObject_7() { return &___m_InstancedGameObject_7; }
	inline void set_m_InstancedGameObject_7(GameObject_t1113636619 * value)
	{
		___m_InstancedGameObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_InstancedGameObject_7), value);
	}

	inline static int32_t get_offset_of_m_Flags_8() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_Flags_8)); }
	inline int32_t get_m_Flags_8() const { return ___m_Flags_8; }
	inline int32_t* get_address_of_m_Flags_8() { return &___m_Flags_8; }
	inline void set_m_Flags_8(int32_t value)
	{
		___m_Flags_8 = value;
	}

	inline static int32_t get_offset_of_m_ColliderType_9() { return static_cast<int32_t>(offsetof(Tile_t1378929773, ___m_ColliderType_9)); }
	inline int32_t get_m_ColliderType_9() const { return ___m_ColliderType_9; }
	inline int32_t* get_address_of_m_ColliderType_9() { return &___m_ColliderType_9; }
	inline void set_m_ColliderType_9(int32_t value)
	{
		___m_ColliderType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILE_T1378929773_H
#ifndef COLLISIONEVENTER_T1111808458_H
#define COLLISIONEVENTER_T1111808458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CollisionEventer
struct  CollisionEventer_t1111808458  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<CollisionEventer/TriggerListener> CollisionEventer::enterListeners
	List_1_t2587299505 * ___enterListeners_4;
	// System.Collections.Generic.List`1<CollisionEventer/TriggerListener> CollisionEventer::exitListeners
	List_1_t2587299505 * ___exitListeners_5;

public:
	inline static int32_t get_offset_of_enterListeners_4() { return static_cast<int32_t>(offsetof(CollisionEventer_t1111808458, ___enterListeners_4)); }
	inline List_1_t2587299505 * get_enterListeners_4() const { return ___enterListeners_4; }
	inline List_1_t2587299505 ** get_address_of_enterListeners_4() { return &___enterListeners_4; }
	inline void set_enterListeners_4(List_1_t2587299505 * value)
	{
		___enterListeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___enterListeners_4), value);
	}

	inline static int32_t get_offset_of_exitListeners_5() { return static_cast<int32_t>(offsetof(CollisionEventer_t1111808458, ___exitListeners_5)); }
	inline List_1_t2587299505 * get_exitListeners_5() const { return ___exitListeners_5; }
	inline List_1_t2587299505 ** get_address_of_exitListeners_5() { return &___exitListeners_5; }
	inline void set_exitListeners_5(List_1_t2587299505 * value)
	{
		___exitListeners_5 = value;
		Il2CppCodeGenWriteBarrier((&___exitListeners_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLISIONEVENTER_T1111808458_H
#ifndef DEATHMENU_T1753622066_H
#define DEATHMENU_T1753622066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathMenu
struct  DeathMenu_t1753622066  : public MonoBehaviour_t3962482529
{
public:
	// System.Single DeathMenu::<menuDuration>k__BackingField
	float ___U3CmenuDurationU3Ek__BackingField_4;
	// UnityEngine.GameObject DeathMenu::main
	GameObject_t1113636619 * ___main_5;
	// UnityEngine.GameObject DeathMenu::player
	GameObject_t1113636619 * ___player_6;
	// Player DeathMenu::playerComponent
	Player_t3266647312 * ___playerComponent_7;
	// UnityEngine.UI.Text DeathMenu::score
	Text_t1901882714 * ___score_8;
	// System.Single DeathMenu::<time>k__BackingField
	float ___U3CtimeU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CmenuDurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___U3CmenuDurationU3Ek__BackingField_4)); }
	inline float get_U3CmenuDurationU3Ek__BackingField_4() const { return ___U3CmenuDurationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CmenuDurationU3Ek__BackingField_4() { return &___U3CmenuDurationU3Ek__BackingField_4; }
	inline void set_U3CmenuDurationU3Ek__BackingField_4(float value)
	{
		___U3CmenuDurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_main_5() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___main_5)); }
	inline GameObject_t1113636619 * get_main_5() const { return ___main_5; }
	inline GameObject_t1113636619 ** get_address_of_main_5() { return &___main_5; }
	inline void set_main_5(GameObject_t1113636619 * value)
	{
		___main_5 = value;
		Il2CppCodeGenWriteBarrier((&___main_5), value);
	}

	inline static int32_t get_offset_of_player_6() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___player_6)); }
	inline GameObject_t1113636619 * get_player_6() const { return ___player_6; }
	inline GameObject_t1113636619 ** get_address_of_player_6() { return &___player_6; }
	inline void set_player_6(GameObject_t1113636619 * value)
	{
		___player_6 = value;
		Il2CppCodeGenWriteBarrier((&___player_6), value);
	}

	inline static int32_t get_offset_of_playerComponent_7() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___playerComponent_7)); }
	inline Player_t3266647312 * get_playerComponent_7() const { return ___playerComponent_7; }
	inline Player_t3266647312 ** get_address_of_playerComponent_7() { return &___playerComponent_7; }
	inline void set_playerComponent_7(Player_t3266647312 * value)
	{
		___playerComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___playerComponent_7), value);
	}

	inline static int32_t get_offset_of_score_8() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___score_8)); }
	inline Text_t1901882714 * get_score_8() const { return ___score_8; }
	inline Text_t1901882714 ** get_address_of_score_8() { return &___score_8; }
	inline void set_score_8(Text_t1901882714 * value)
	{
		___score_8 = value;
		Il2CppCodeGenWriteBarrier((&___score_8), value);
	}

	inline static int32_t get_offset_of_U3CtimeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DeathMenu_t1753622066, ___U3CtimeU3Ek__BackingField_9)); }
	inline float get_U3CtimeU3Ek__BackingField_9() const { return ___U3CtimeU3Ek__BackingField_9; }
	inline float* get_address_of_U3CtimeU3Ek__BackingField_9() { return &___U3CtimeU3Ek__BackingField_9; }
	inline void set_U3CtimeU3Ek__BackingField_9(float value)
	{
		___U3CtimeU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHMENU_T1753622066_H
#ifndef GAMESCREEN_T1800529819_H
#define GAMESCREEN_T1800529819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameScreen
struct  GameScreen_t1800529819  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject GameScreen::player
	GameObject_t1113636619 * ___player_4;
	// Player GameScreen::playerComponent
	Player_t3266647312 * ___playerComponent_5;
	// UnityEngine.GameObject GameScreen::deathScreen
	GameObject_t1113636619 * ___deathScreen_6;
	// UnityEngine.GameObject GameScreen::level
	GameObject_t1113636619 * ___level_7;
	// Level GameScreen::levelComponent
	Level_t2237665516 * ___levelComponent_8;
	// UnityEngine.UI.Text GameScreen::score
	Text_t1901882714 * ___score_9;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_playerComponent_5() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___playerComponent_5)); }
	inline Player_t3266647312 * get_playerComponent_5() const { return ___playerComponent_5; }
	inline Player_t3266647312 ** get_address_of_playerComponent_5() { return &___playerComponent_5; }
	inline void set_playerComponent_5(Player_t3266647312 * value)
	{
		___playerComponent_5 = value;
		Il2CppCodeGenWriteBarrier((&___playerComponent_5), value);
	}

	inline static int32_t get_offset_of_deathScreen_6() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___deathScreen_6)); }
	inline GameObject_t1113636619 * get_deathScreen_6() const { return ___deathScreen_6; }
	inline GameObject_t1113636619 ** get_address_of_deathScreen_6() { return &___deathScreen_6; }
	inline void set_deathScreen_6(GameObject_t1113636619 * value)
	{
		___deathScreen_6 = value;
		Il2CppCodeGenWriteBarrier((&___deathScreen_6), value);
	}

	inline static int32_t get_offset_of_level_7() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___level_7)); }
	inline GameObject_t1113636619 * get_level_7() const { return ___level_7; }
	inline GameObject_t1113636619 ** get_address_of_level_7() { return &___level_7; }
	inline void set_level_7(GameObject_t1113636619 * value)
	{
		___level_7 = value;
		Il2CppCodeGenWriteBarrier((&___level_7), value);
	}

	inline static int32_t get_offset_of_levelComponent_8() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___levelComponent_8)); }
	inline Level_t2237665516 * get_levelComponent_8() const { return ___levelComponent_8; }
	inline Level_t2237665516 ** get_address_of_levelComponent_8() { return &___levelComponent_8; }
	inline void set_levelComponent_8(Level_t2237665516 * value)
	{
		___levelComponent_8 = value;
		Il2CppCodeGenWriteBarrier((&___levelComponent_8), value);
	}

	inline static int32_t get_offset_of_score_9() { return static_cast<int32_t>(offsetof(GameScreen_t1800529819, ___score_9)); }
	inline Text_t1901882714 * get_score_9() const { return ___score_9; }
	inline Text_t1901882714 ** get_address_of_score_9() { return &___score_9; }
	inline void set_score_9(Text_t1901882714 * value)
	{
		___score_9 = value;
		Il2CppCodeGenWriteBarrier((&___score_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESCREEN_T1800529819_H
#ifndef LEVEL_T2237665516_H
#define LEVEL_T2237665516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Level
struct  Level_t2237665516  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Tilemaps.Tile Level::floor
	Tile_t1378929773 * ___floor_4;
	// UnityEngine.Tilemaps.Tile Level::wall
	Tile_t1378929773 * ___wall_5;
	// UnityEngine.Tilemaps.Tile Level::water
	Tile_t1378929773 * ___water_6;
	// UnityEngine.Tilemaps.Tilemap Level::floorMap
	Tilemap_t1578933799 * ___floorMap_7;
	// UnityEngine.Tilemaps.Tilemap Level::wallMap
	Tilemap_t1578933799 * ___wallMap_8;
	// Player Level::player
	Player_t3266647312 * ___player_9;
	// System.Int32 Level::size
	int32_t ___size_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Level::entities
	List_1_t2585711361 * ___entities_11;

public:
	inline static int32_t get_offset_of_floor_4() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___floor_4)); }
	inline Tile_t1378929773 * get_floor_4() const { return ___floor_4; }
	inline Tile_t1378929773 ** get_address_of_floor_4() { return &___floor_4; }
	inline void set_floor_4(Tile_t1378929773 * value)
	{
		___floor_4 = value;
		Il2CppCodeGenWriteBarrier((&___floor_4), value);
	}

	inline static int32_t get_offset_of_wall_5() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___wall_5)); }
	inline Tile_t1378929773 * get_wall_5() const { return ___wall_5; }
	inline Tile_t1378929773 ** get_address_of_wall_5() { return &___wall_5; }
	inline void set_wall_5(Tile_t1378929773 * value)
	{
		___wall_5 = value;
		Il2CppCodeGenWriteBarrier((&___wall_5), value);
	}

	inline static int32_t get_offset_of_water_6() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___water_6)); }
	inline Tile_t1378929773 * get_water_6() const { return ___water_6; }
	inline Tile_t1378929773 ** get_address_of_water_6() { return &___water_6; }
	inline void set_water_6(Tile_t1378929773 * value)
	{
		___water_6 = value;
		Il2CppCodeGenWriteBarrier((&___water_6), value);
	}

	inline static int32_t get_offset_of_floorMap_7() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___floorMap_7)); }
	inline Tilemap_t1578933799 * get_floorMap_7() const { return ___floorMap_7; }
	inline Tilemap_t1578933799 ** get_address_of_floorMap_7() { return &___floorMap_7; }
	inline void set_floorMap_7(Tilemap_t1578933799 * value)
	{
		___floorMap_7 = value;
		Il2CppCodeGenWriteBarrier((&___floorMap_7), value);
	}

	inline static int32_t get_offset_of_wallMap_8() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___wallMap_8)); }
	inline Tilemap_t1578933799 * get_wallMap_8() const { return ___wallMap_8; }
	inline Tilemap_t1578933799 ** get_address_of_wallMap_8() { return &___wallMap_8; }
	inline void set_wallMap_8(Tilemap_t1578933799 * value)
	{
		___wallMap_8 = value;
		Il2CppCodeGenWriteBarrier((&___wallMap_8), value);
	}

	inline static int32_t get_offset_of_player_9() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___player_9)); }
	inline Player_t3266647312 * get_player_9() const { return ___player_9; }
	inline Player_t3266647312 ** get_address_of_player_9() { return &___player_9; }
	inline void set_player_9(Player_t3266647312 * value)
	{
		___player_9 = value;
		Il2CppCodeGenWriteBarrier((&___player_9), value);
	}

	inline static int32_t get_offset_of_size_10() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___size_10)); }
	inline int32_t get_size_10() const { return ___size_10; }
	inline int32_t* get_address_of_size_10() { return &___size_10; }
	inline void set_size_10(int32_t value)
	{
		___size_10 = value;
	}

	inline static int32_t get_offset_of_entities_11() { return static_cast<int32_t>(offsetof(Level_t2237665516, ___entities_11)); }
	inline List_1_t2585711361 * get_entities_11() const { return ___entities_11; }
	inline List_1_t2585711361 ** get_address_of_entities_11() { return &___entities_11; }
	inline void set_entities_11(List_1_t2585711361 * value)
	{
		___entities_11 = value;
		Il2CppCodeGenWriteBarrier((&___entities_11), value);
	}
};

struct Level_t2237665516_StaticFields
{
public:
	// System.Action`1<UnityEngine.GameObject> Level::<>f__am$cache0
	Action_1_t1286104214 * ___U3CU3Ef__amU24cache0_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(Level_t2237665516_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Action_1_t1286104214 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Action_1_t1286104214 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Action_1_t1286104214 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEVEL_T2237665516_H
#ifndef MAINMENU_T3798339593_H
#define MAINMENU_T3798339593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainMenu
struct  MainMenu_t3798339593  : public MonoBehaviour_t3962482529
{
public:
	// System.Single MainMenu::<menuDuration>k__BackingField
	float ___U3CmenuDurationU3Ek__BackingField_4;
	// UnityEngine.GameObject MainMenu::game
	GameObject_t1113636619 * ___game_5;
	// System.Single MainMenu::<blinkTime>k__BackingField
	float ___U3CblinkTimeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CmenuDurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MainMenu_t3798339593, ___U3CmenuDurationU3Ek__BackingField_4)); }
	inline float get_U3CmenuDurationU3Ek__BackingField_4() const { return ___U3CmenuDurationU3Ek__BackingField_4; }
	inline float* get_address_of_U3CmenuDurationU3Ek__BackingField_4() { return &___U3CmenuDurationU3Ek__BackingField_4; }
	inline void set_U3CmenuDurationU3Ek__BackingField_4(float value)
	{
		___U3CmenuDurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_game_5() { return static_cast<int32_t>(offsetof(MainMenu_t3798339593, ___game_5)); }
	inline GameObject_t1113636619 * get_game_5() const { return ___game_5; }
	inline GameObject_t1113636619 ** get_address_of_game_5() { return &___game_5; }
	inline void set_game_5(GameObject_t1113636619 * value)
	{
		___game_5 = value;
		Il2CppCodeGenWriteBarrier((&___game_5), value);
	}

	inline static int32_t get_offset_of_U3CblinkTimeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MainMenu_t3798339593, ___U3CblinkTimeU3Ek__BackingField_6)); }
	inline float get_U3CblinkTimeU3Ek__BackingField_6() const { return ___U3CblinkTimeU3Ek__BackingField_6; }
	inline float* get_address_of_U3CblinkTimeU3Ek__BackingField_6() { return &___U3CblinkTimeU3Ek__BackingField_6; }
	inline void set_U3CblinkTimeU3Ek__BackingField_6(float value)
	{
		___U3CblinkTimeU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T3798339593_H
#ifndef MENUCAMERA_T4282292518_H
#define MENUCAMERA_T4282292518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuCamera
struct  MenuCamera_t4282292518  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject MenuCamera::menu
	GameObject_t1113636619 * ___menu_4;
	// UnityEngine.Material MenuCamera::menuMaterial
	Material_t340375123 * ___menuMaterial_5;

public:
	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(MenuCamera_t4282292518, ___menu_4)); }
	inline GameObject_t1113636619 * get_menu_4() const { return ___menu_4; }
	inline GameObject_t1113636619 ** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(GameObject_t1113636619 * value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}

	inline static int32_t get_offset_of_menuMaterial_5() { return static_cast<int32_t>(offsetof(MenuCamera_t4282292518, ___menuMaterial_5)); }
	inline Material_t340375123 * get_menuMaterial_5() const { return ___menuMaterial_5; }
	inline Material_t340375123 ** get_address_of_menuMaterial_5() { return &___menuMaterial_5; }
	inline void set_menuMaterial_5(Material_t340375123 * value)
	{
		___menuMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUCAMERA_T4282292518_H
#ifndef MONSTER_T1049719775_H
#define MONSTER_T1049719775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Monster
struct  Monster_t1049719775  : public MonoBehaviour_t3962482529
{
public:
	// System.Single Monster::maxStateQueue
	float ___maxStateQueue_4;
	// System.Single Monster::minStateQueue
	float ___minStateQueue_5;
	// System.Single Monster::fov
	float ___fov_6;
	// UnityEngine.Rigidbody2D Monster::rigidcollider
	Rigidbody2D_t939494601 * ___rigidcollider_7;
	// UnityEngine.CircleCollider2D Monster::vision
	CircleCollider2D_t662546754 * ___vision_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Monster::aware
	List_1_t2585711361 * ___aware_9;
	// Monster/MonsterState Monster::state
	int32_t ___state_10;
	// System.Single Monster::stateRemaining
	float ___stateRemaining_11;
	// System.Boolean Monster::rotationDirection
	bool ___rotationDirection_12;
	// UnityEngine.GameObject Monster::chargeTarget
	GameObject_t1113636619 * ___chargeTarget_13;
	// UnityEngine.AudioSource Monster::howl
	AudioSource_t3935305588 * ___howl_14;

public:
	inline static int32_t get_offset_of_maxStateQueue_4() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___maxStateQueue_4)); }
	inline float get_maxStateQueue_4() const { return ___maxStateQueue_4; }
	inline float* get_address_of_maxStateQueue_4() { return &___maxStateQueue_4; }
	inline void set_maxStateQueue_4(float value)
	{
		___maxStateQueue_4 = value;
	}

	inline static int32_t get_offset_of_minStateQueue_5() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___minStateQueue_5)); }
	inline float get_minStateQueue_5() const { return ___minStateQueue_5; }
	inline float* get_address_of_minStateQueue_5() { return &___minStateQueue_5; }
	inline void set_minStateQueue_5(float value)
	{
		___minStateQueue_5 = value;
	}

	inline static int32_t get_offset_of_fov_6() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___fov_6)); }
	inline float get_fov_6() const { return ___fov_6; }
	inline float* get_address_of_fov_6() { return &___fov_6; }
	inline void set_fov_6(float value)
	{
		___fov_6 = value;
	}

	inline static int32_t get_offset_of_rigidcollider_7() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___rigidcollider_7)); }
	inline Rigidbody2D_t939494601 * get_rigidcollider_7() const { return ___rigidcollider_7; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidcollider_7() { return &___rigidcollider_7; }
	inline void set_rigidcollider_7(Rigidbody2D_t939494601 * value)
	{
		___rigidcollider_7 = value;
		Il2CppCodeGenWriteBarrier((&___rigidcollider_7), value);
	}

	inline static int32_t get_offset_of_vision_8() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___vision_8)); }
	inline CircleCollider2D_t662546754 * get_vision_8() const { return ___vision_8; }
	inline CircleCollider2D_t662546754 ** get_address_of_vision_8() { return &___vision_8; }
	inline void set_vision_8(CircleCollider2D_t662546754 * value)
	{
		___vision_8 = value;
		Il2CppCodeGenWriteBarrier((&___vision_8), value);
	}

	inline static int32_t get_offset_of_aware_9() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___aware_9)); }
	inline List_1_t2585711361 * get_aware_9() const { return ___aware_9; }
	inline List_1_t2585711361 ** get_address_of_aware_9() { return &___aware_9; }
	inline void set_aware_9(List_1_t2585711361 * value)
	{
		___aware_9 = value;
		Il2CppCodeGenWriteBarrier((&___aware_9), value);
	}

	inline static int32_t get_offset_of_state_10() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___state_10)); }
	inline int32_t get_state_10() const { return ___state_10; }
	inline int32_t* get_address_of_state_10() { return &___state_10; }
	inline void set_state_10(int32_t value)
	{
		___state_10 = value;
	}

	inline static int32_t get_offset_of_stateRemaining_11() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___stateRemaining_11)); }
	inline float get_stateRemaining_11() const { return ___stateRemaining_11; }
	inline float* get_address_of_stateRemaining_11() { return &___stateRemaining_11; }
	inline void set_stateRemaining_11(float value)
	{
		___stateRemaining_11 = value;
	}

	inline static int32_t get_offset_of_rotationDirection_12() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___rotationDirection_12)); }
	inline bool get_rotationDirection_12() const { return ___rotationDirection_12; }
	inline bool* get_address_of_rotationDirection_12() { return &___rotationDirection_12; }
	inline void set_rotationDirection_12(bool value)
	{
		___rotationDirection_12 = value;
	}

	inline static int32_t get_offset_of_chargeTarget_13() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___chargeTarget_13)); }
	inline GameObject_t1113636619 * get_chargeTarget_13() const { return ___chargeTarget_13; }
	inline GameObject_t1113636619 ** get_address_of_chargeTarget_13() { return &___chargeTarget_13; }
	inline void set_chargeTarget_13(GameObject_t1113636619 * value)
	{
		___chargeTarget_13 = value;
		Il2CppCodeGenWriteBarrier((&___chargeTarget_13), value);
	}

	inline static int32_t get_offset_of_howl_14() { return static_cast<int32_t>(offsetof(Monster_t1049719775, ___howl_14)); }
	inline AudioSource_t3935305588 * get_howl_14() const { return ___howl_14; }
	inline AudioSource_t3935305588 ** get_address_of_howl_14() { return &___howl_14; }
	inline void set_howl_14(AudioSource_t3935305588 * value)
	{
		___howl_14 = value;
		Il2CppCodeGenWriteBarrier((&___howl_14), value);
	}
};

struct Monster_t1049719775_StaticFields
{
public:
	// System.Comparison`1<UnityEngine.RaycastHit2D> Monster::<>f__am$cache0
	Comparison_1_t2054513168 * ___U3CU3Ef__amU24cache0_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(Monster_t1049719775_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Comparison_1_t2054513168 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Comparison_1_t2054513168 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Comparison_1_t2054513168 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONSTER_T1049719775_H
#ifndef PLAYER_T3266647312_H
#define PLAYER_T3266647312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t3266647312  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rigidbody2D Player::rigidcollider
	Rigidbody2D_t939494601 * ___rigidcollider_4;
	// UnityEngine.CircleCollider2D Player::vision
	CircleCollider2D_t662546754 * ___vision_5;
	// System.Single Player::<stress>k__BackingField
	float ___U3CstressU3Ek__BackingField_6;
	// System.Single Player::stressVelocity
	float ___stressVelocity_7;
	// System.Single Player::<blink>k__BackingField
	float ___U3CblinkU3Ek__BackingField_8;
	// System.Single Player::blinkTime
	float ___blinkTime_9;
	// System.Single Player::graceTimer
	float ___graceTimer_10;
	// System.Int32 Player::<poolsDiscovered>k__BackingField
	int32_t ___U3CpoolsDiscoveredU3Ek__BackingField_11;
	// Water Player::pool
	Water_t1083516957 * ___pool_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Player::monsters
	List_1_t2585711361 * ___monsters_13;
	// UnityEngine.AudioSource Player::heartbeat
	AudioSource_t3935305588 * ___heartbeat_14;
	// System.Single Player::heartbeatCooldown
	float ___heartbeatCooldown_15;
	// System.Single Player::minHeartrate
	float ___minHeartrate_16;
	// System.Single Player::maxHeartrate
	float ___maxHeartrate_17;
	// System.Single Player::speed
	float ___speed_18;
	// System.Single Player::recoverDelta
	float ___recoverDelta_19;
	// System.Single Player::blinkPenalty
	float ___blinkPenalty_20;
	// System.Single Player::restingStressVelocity
	float ___restingStressVelocity_21;
	// System.Single Player::waterBonus
	float ___waterBonus_22;
	// System.Single Player::waterStressVelocity
	float ___waterStressVelocity_23;
	// System.Single Player::gracePeriod
	float ___gracePeriod_24;

public:
	inline static int32_t get_offset_of_rigidcollider_4() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___rigidcollider_4)); }
	inline Rigidbody2D_t939494601 * get_rigidcollider_4() const { return ___rigidcollider_4; }
	inline Rigidbody2D_t939494601 ** get_address_of_rigidcollider_4() { return &___rigidcollider_4; }
	inline void set_rigidcollider_4(Rigidbody2D_t939494601 * value)
	{
		___rigidcollider_4 = value;
		Il2CppCodeGenWriteBarrier((&___rigidcollider_4), value);
	}

	inline static int32_t get_offset_of_vision_5() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___vision_5)); }
	inline CircleCollider2D_t662546754 * get_vision_5() const { return ___vision_5; }
	inline CircleCollider2D_t662546754 ** get_address_of_vision_5() { return &___vision_5; }
	inline void set_vision_5(CircleCollider2D_t662546754 * value)
	{
		___vision_5 = value;
		Il2CppCodeGenWriteBarrier((&___vision_5), value);
	}

	inline static int32_t get_offset_of_U3CstressU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___U3CstressU3Ek__BackingField_6)); }
	inline float get_U3CstressU3Ek__BackingField_6() const { return ___U3CstressU3Ek__BackingField_6; }
	inline float* get_address_of_U3CstressU3Ek__BackingField_6() { return &___U3CstressU3Ek__BackingField_6; }
	inline void set_U3CstressU3Ek__BackingField_6(float value)
	{
		___U3CstressU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_stressVelocity_7() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___stressVelocity_7)); }
	inline float get_stressVelocity_7() const { return ___stressVelocity_7; }
	inline float* get_address_of_stressVelocity_7() { return &___stressVelocity_7; }
	inline void set_stressVelocity_7(float value)
	{
		___stressVelocity_7 = value;
	}

	inline static int32_t get_offset_of_U3CblinkU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___U3CblinkU3Ek__BackingField_8)); }
	inline float get_U3CblinkU3Ek__BackingField_8() const { return ___U3CblinkU3Ek__BackingField_8; }
	inline float* get_address_of_U3CblinkU3Ek__BackingField_8() { return &___U3CblinkU3Ek__BackingField_8; }
	inline void set_U3CblinkU3Ek__BackingField_8(float value)
	{
		___U3CblinkU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_blinkTime_9() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___blinkTime_9)); }
	inline float get_blinkTime_9() const { return ___blinkTime_9; }
	inline float* get_address_of_blinkTime_9() { return &___blinkTime_9; }
	inline void set_blinkTime_9(float value)
	{
		___blinkTime_9 = value;
	}

	inline static int32_t get_offset_of_graceTimer_10() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___graceTimer_10)); }
	inline float get_graceTimer_10() const { return ___graceTimer_10; }
	inline float* get_address_of_graceTimer_10() { return &___graceTimer_10; }
	inline void set_graceTimer_10(float value)
	{
		___graceTimer_10 = value;
	}

	inline static int32_t get_offset_of_U3CpoolsDiscoveredU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___U3CpoolsDiscoveredU3Ek__BackingField_11)); }
	inline int32_t get_U3CpoolsDiscoveredU3Ek__BackingField_11() const { return ___U3CpoolsDiscoveredU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CpoolsDiscoveredU3Ek__BackingField_11() { return &___U3CpoolsDiscoveredU3Ek__BackingField_11; }
	inline void set_U3CpoolsDiscoveredU3Ek__BackingField_11(int32_t value)
	{
		___U3CpoolsDiscoveredU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_pool_12() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___pool_12)); }
	inline Water_t1083516957 * get_pool_12() const { return ___pool_12; }
	inline Water_t1083516957 ** get_address_of_pool_12() { return &___pool_12; }
	inline void set_pool_12(Water_t1083516957 * value)
	{
		___pool_12 = value;
		Il2CppCodeGenWriteBarrier((&___pool_12), value);
	}

	inline static int32_t get_offset_of_monsters_13() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___monsters_13)); }
	inline List_1_t2585711361 * get_monsters_13() const { return ___monsters_13; }
	inline List_1_t2585711361 ** get_address_of_monsters_13() { return &___monsters_13; }
	inline void set_monsters_13(List_1_t2585711361 * value)
	{
		___monsters_13 = value;
		Il2CppCodeGenWriteBarrier((&___monsters_13), value);
	}

	inline static int32_t get_offset_of_heartbeat_14() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___heartbeat_14)); }
	inline AudioSource_t3935305588 * get_heartbeat_14() const { return ___heartbeat_14; }
	inline AudioSource_t3935305588 ** get_address_of_heartbeat_14() { return &___heartbeat_14; }
	inline void set_heartbeat_14(AudioSource_t3935305588 * value)
	{
		___heartbeat_14 = value;
		Il2CppCodeGenWriteBarrier((&___heartbeat_14), value);
	}

	inline static int32_t get_offset_of_heartbeatCooldown_15() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___heartbeatCooldown_15)); }
	inline float get_heartbeatCooldown_15() const { return ___heartbeatCooldown_15; }
	inline float* get_address_of_heartbeatCooldown_15() { return &___heartbeatCooldown_15; }
	inline void set_heartbeatCooldown_15(float value)
	{
		___heartbeatCooldown_15 = value;
	}

	inline static int32_t get_offset_of_minHeartrate_16() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___minHeartrate_16)); }
	inline float get_minHeartrate_16() const { return ___minHeartrate_16; }
	inline float* get_address_of_minHeartrate_16() { return &___minHeartrate_16; }
	inline void set_minHeartrate_16(float value)
	{
		___minHeartrate_16 = value;
	}

	inline static int32_t get_offset_of_maxHeartrate_17() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___maxHeartrate_17)); }
	inline float get_maxHeartrate_17() const { return ___maxHeartrate_17; }
	inline float* get_address_of_maxHeartrate_17() { return &___maxHeartrate_17; }
	inline void set_maxHeartrate_17(float value)
	{
		___maxHeartrate_17 = value;
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___speed_18)); }
	inline float get_speed_18() const { return ___speed_18; }
	inline float* get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(float value)
	{
		___speed_18 = value;
	}

	inline static int32_t get_offset_of_recoverDelta_19() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___recoverDelta_19)); }
	inline float get_recoverDelta_19() const { return ___recoverDelta_19; }
	inline float* get_address_of_recoverDelta_19() { return &___recoverDelta_19; }
	inline void set_recoverDelta_19(float value)
	{
		___recoverDelta_19 = value;
	}

	inline static int32_t get_offset_of_blinkPenalty_20() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___blinkPenalty_20)); }
	inline float get_blinkPenalty_20() const { return ___blinkPenalty_20; }
	inline float* get_address_of_blinkPenalty_20() { return &___blinkPenalty_20; }
	inline void set_blinkPenalty_20(float value)
	{
		___blinkPenalty_20 = value;
	}

	inline static int32_t get_offset_of_restingStressVelocity_21() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___restingStressVelocity_21)); }
	inline float get_restingStressVelocity_21() const { return ___restingStressVelocity_21; }
	inline float* get_address_of_restingStressVelocity_21() { return &___restingStressVelocity_21; }
	inline void set_restingStressVelocity_21(float value)
	{
		___restingStressVelocity_21 = value;
	}

	inline static int32_t get_offset_of_waterBonus_22() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___waterBonus_22)); }
	inline float get_waterBonus_22() const { return ___waterBonus_22; }
	inline float* get_address_of_waterBonus_22() { return &___waterBonus_22; }
	inline void set_waterBonus_22(float value)
	{
		___waterBonus_22 = value;
	}

	inline static int32_t get_offset_of_waterStressVelocity_23() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___waterStressVelocity_23)); }
	inline float get_waterStressVelocity_23() const { return ___waterStressVelocity_23; }
	inline float* get_address_of_waterStressVelocity_23() { return &___waterStressVelocity_23; }
	inline void set_waterStressVelocity_23(float value)
	{
		___waterStressVelocity_23 = value;
	}

	inline static int32_t get_offset_of_gracePeriod_24() { return static_cast<int32_t>(offsetof(Player_t3266647312, ___gracePeriod_24)); }
	inline float get_gracePeriod_24() const { return ___gracePeriod_24; }
	inline float* get_address_of_gracePeriod_24() { return &___gracePeriod_24; }
	inline void set_gracePeriod_24(float value)
	{
		___gracePeriod_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T3266647312_H
#ifndef PLAYERCAMERA_T1622178205_H
#define PLAYERCAMERA_T1622178205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerCamera
struct  PlayerCamera_t1622178205  : public MonoBehaviour_t3962482529
{
public:
	// Player PlayerCamera::player
	Player_t3266647312 * ___player_4;
	// UnityEngine.Material PlayerCamera::cameraMaterial
	Material_t340375123 * ___cameraMaterial_5;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(PlayerCamera_t1622178205, ___player_4)); }
	inline Player_t3266647312 * get_player_4() const { return ___player_4; }
	inline Player_t3266647312 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(Player_t3266647312 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_cameraMaterial_5() { return static_cast<int32_t>(offsetof(PlayerCamera_t1622178205, ___cameraMaterial_5)); }
	inline Material_t340375123 * get_cameraMaterial_5() const { return ___cameraMaterial_5; }
	inline Material_t340375123 ** get_address_of_cameraMaterial_5() { return &___cameraMaterial_5; }
	inline void set_cameraMaterial_5(Material_t340375123 * value)
	{
		___cameraMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___cameraMaterial_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERCAMERA_T1622178205_H
#ifndef AUDIOSOURCE_T3935305588_H
#define AUDIOSOURCE_T3935305588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AudioSource
struct  AudioSource_t3935305588  : public AudioBehaviour_t2879336574
{
public:
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::spatializerExtension
	AudioSourceExtension_t3064908834 * ___spatializerExtension_4;
	// UnityEngine.AudioSourceExtension UnityEngine.AudioSource::ambisonicExtension
	AudioSourceExtension_t3064908834 * ___ambisonicExtension_5;

public:
	inline static int32_t get_offset_of_spatializerExtension_4() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___spatializerExtension_4)); }
	inline AudioSourceExtension_t3064908834 * get_spatializerExtension_4() const { return ___spatializerExtension_4; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_spatializerExtension_4() { return &___spatializerExtension_4; }
	inline void set_spatializerExtension_4(AudioSourceExtension_t3064908834 * value)
	{
		___spatializerExtension_4 = value;
		Il2CppCodeGenWriteBarrier((&___spatializerExtension_4), value);
	}

	inline static int32_t get_offset_of_ambisonicExtension_5() { return static_cast<int32_t>(offsetof(AudioSource_t3935305588, ___ambisonicExtension_5)); }
	inline AudioSourceExtension_t3064908834 * get_ambisonicExtension_5() const { return ___ambisonicExtension_5; }
	inline AudioSourceExtension_t3064908834 ** get_address_of_ambisonicExtension_5() { return &___ambisonicExtension_5; }
	inline void set_ambisonicExtension_5(AudioSourceExtension_t3064908834 * value)
	{
		___ambisonicExtension_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambisonicExtension_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOSOURCE_T3935305588_H
#ifndef BOXCOLLIDER2D_T3581341831_H
#define BOXCOLLIDER2D_T3581341831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.BoxCollider2D
struct  BoxCollider2D_t3581341831  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOXCOLLIDER2D_T3581341831_H
#ifndef CIRCLECOLLIDER2D_T662546754_H
#define CIRCLECOLLIDER2D_T662546754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CircleCollider2D
struct  CircleCollider2D_t662546754  : public Collider2D_t2806799626
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLECOLLIDER2D_T662546754_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef TILEMAP_T1578933799_H
#define TILEMAP_T1578933799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Tilemaps.Tilemap
struct  Tilemap_t1578933799  : public GridLayout_t754322041
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TILEMAP_T1578933799_H
#ifndef WATER_T1083516957_H
#define WATER_T1083516957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Water
struct  Water_t1083516957  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Water::player
	GameObject_t1113636619 * ___player_4;
	// UnityEngine.GameObject Water::collision
	GameObject_t1113636619 * ___collision_5;
	// UnityEngine.GameObject Water::trigger
	GameObject_t1113636619 * ___trigger_6;
	// Water/Activator Water::activator
	Activator_t2573376075 * ___activator_7;
	// System.Boolean Water::<Active>k__BackingField
	bool ___U3CActiveU3Ek__BackingField_8;
	// System.Boolean Water::<Discovered>k__BackingField
	bool ___U3CDiscoveredU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_player_4() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___player_4)); }
	inline GameObject_t1113636619 * get_player_4() const { return ___player_4; }
	inline GameObject_t1113636619 ** get_address_of_player_4() { return &___player_4; }
	inline void set_player_4(GameObject_t1113636619 * value)
	{
		___player_4 = value;
		Il2CppCodeGenWriteBarrier((&___player_4), value);
	}

	inline static int32_t get_offset_of_collision_5() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___collision_5)); }
	inline GameObject_t1113636619 * get_collision_5() const { return ___collision_5; }
	inline GameObject_t1113636619 ** get_address_of_collision_5() { return &___collision_5; }
	inline void set_collision_5(GameObject_t1113636619 * value)
	{
		___collision_5 = value;
		Il2CppCodeGenWriteBarrier((&___collision_5), value);
	}

	inline static int32_t get_offset_of_trigger_6() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___trigger_6)); }
	inline GameObject_t1113636619 * get_trigger_6() const { return ___trigger_6; }
	inline GameObject_t1113636619 ** get_address_of_trigger_6() { return &___trigger_6; }
	inline void set_trigger_6(GameObject_t1113636619 * value)
	{
		___trigger_6 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_6), value);
	}

	inline static int32_t get_offset_of_activator_7() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___activator_7)); }
	inline Activator_t2573376075 * get_activator_7() const { return ___activator_7; }
	inline Activator_t2573376075 ** get_address_of_activator_7() { return &___activator_7; }
	inline void set_activator_7(Activator_t2573376075 * value)
	{
		___activator_7 = value;
		Il2CppCodeGenWriteBarrier((&___activator_7), value);
	}

	inline static int32_t get_offset_of_U3CActiveU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___U3CActiveU3Ek__BackingField_8)); }
	inline bool get_U3CActiveU3Ek__BackingField_8() const { return ___U3CActiveU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CActiveU3Ek__BackingField_8() { return &___U3CActiveU3Ek__BackingField_8; }
	inline void set_U3CActiveU3Ek__BackingField_8(bool value)
	{
		___U3CActiveU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CDiscoveredU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Water_t1083516957, ___U3CDiscoveredU3Ek__BackingField_9)); }
	inline bool get_U3CDiscoveredU3Ek__BackingField_9() const { return ___U3CDiscoveredU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CDiscoveredU3Ek__BackingField_9() { return &___U3CDiscoveredU3Ek__BackingField_9; }
	inline void set_U3CDiscoveredU3Ek__BackingField_9(bool value)
	{
		___U3CDiscoveredU3Ek__BackingField_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WATER_T1083516957_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Level/TileType[0...,0...]
struct TileTypeU5B0___U2C0___U5D_t2292257856  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RaycastHit2D_t2279581989  m_Items[1];

public:
	inline RaycastHit2D_t2279581989  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit2D_t2279581989  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2146457487  List_1_GetEnumerator_m816315209_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m337713592_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1_ForEach_m3737504377_gshared (List_1_t257213610 * __this, Action_1_t3252573759 * p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_m1135049463_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void System.Comparison`1<UnityEngine.RaycastHit2D>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Comparison_1__ctor_m1180004629_gshared (Comparison_1_t2054513168 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Array::Sort<UnityEngine.RaycastHit2D>(!!0[],System.Comparison`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR void Array_Sort_TisRaycastHit2D_t2279581989_m3365424645_gshared (RuntimeObject * __this /* static, unused */, RaycastHit2DU5BU5D_t4286651560* p0, Comparison_1_t2054513168 * p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m2390619627_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);

// System.Void System.Collections.Generic.List`1<CollisionEventer/TriggerListener>::.ctor()
inline void List_1__ctor_m3280379170 (List_1_t2587299505 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2587299505 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<CollisionEventer/TriggerListener>::GetEnumerator()
inline Enumerator_t181576086  List_1_GetEnumerator_m3837348380 (List_1_t2587299505 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t181576086  (*) (List_1_t2587299505 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<CollisionEventer/TriggerListener>::get_Current()
inline TriggerListener_t1115224763 * Enumerator_get_Current_m959559447 (Enumerator_t181576086 * __this, const RuntimeMethod* method)
{
	return ((  TriggerListener_t1115224763 * (*) (Enumerator_t181576086 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method);
}
// System.Void CollisionEventer/TriggerListener::Invoke(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void TriggerListener_Invoke_m2431345097 (TriggerListener_t1115224763 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<CollisionEventer/TriggerListener>::MoveNext()
inline bool Enumerator_MoveNext_m1996258908 (Enumerator_t181576086 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t181576086 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<CollisionEventer/TriggerListener>::Dispose()
inline void Enumerator_Dispose_m765708873 (Enumerator_t181576086 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t181576086 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<CollisionEventer/TriggerListener>::Add(!0)
inline void List_1_Add_m1077912546 (List_1_t2587299505 * __this, TriggerListener_t1115224763 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2587299505 *, TriggerListener_t1115224763 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<CollisionEventer/TriggerListener>::Clear()
inline void List_1_Clear_m3309761785 (List_1_t2587299505 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2587299505 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<CollisionEventer>()
inline CollisionEventer_t1111808458 * GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  CollisionEventer_t1111808458 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<CollisionEventer>()
inline CollisionEventer_t1111808458 * GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  CollisionEventer_t1111808458 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m3469369570_gshared)(__this, method);
}
// System.Void CollisionEventer::OnTriggerEnter2D(CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerEnter2D_m3032026089 (CollisionEventer_t1111808458 * __this, TriggerListener_t1115224763 * ___listener0, const RuntimeMethod* method);
// System.Void CollisionEventer::OnTriggerExit2D(CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerExit2D_m61367443 (CollisionEventer_t1111808458 * __this, TriggerListener_t1115224763 * ___listener0, const RuntimeMethod* method);
// System.Void CollisionEventer::ClearTriggerEnter2D()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerEnter2D_m448421859 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method);
// System.Void CollisionEventer::ClearTriggerExit2D()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerExit2D_m704284422 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method);
// System.Void DeathMenu::Init()
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_Init_m3830644225 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t1901882714 * GameObject_GetComponent_TisText_t1901882714_m2114913816 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<Player>()
inline Player_t3266647312 * GameObject_GetComponent_TisPlayer_t3266647312_m4068145281 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Player_t3266647312 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void DeathMenu::set_menuDuration(System.Single)
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_set_menuDuration_m761959042 (DeathMenu_t1753622066 * __this, float ___value0, const RuntimeMethod* method);
// System.Void DeathMenu::set_time(System.Single)
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_set_time_m3325743434 (DeathMenu_t1753622066 * __this, float ___value0, const RuntimeMethod* method);
// System.Int32 Player::get_poolsDiscovered()
extern "C" IL2CPP_METHOD_ATTR int32_t Player_get_poolsDiscovered_m1228037983 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1715369213 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Single DeathMenu::get_time()
extern "C" IL2CPP_METHOD_ATTR float DeathMenu_get_time_m239362606 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" IL2CPP_METHOD_ATTR float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Single DeathMenu::get_menuDuration()
extern "C" IL2CPP_METHOD_ATTR float DeathMenu_get_menuDuration_m1418593762 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<MainMenu>()
inline MainMenu_t3798339593 * GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  MainMenu_t3798339593 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void MainMenu::Init()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Init_m4196376439 (MainMenu_t3798339593 * __this, const RuntimeMethod* method);
// System.Void GameScreen::Init()
extern "C" IL2CPP_METHOD_ATTR void GameScreen_Init_m2409608521 (GameScreen_t1800529819 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Level>()
inline Level_t2237665516 * GameObject_GetComponent_TisLevel_t2237665516_m1206308685 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Level_t2237665516 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void Level::Init()
extern "C" IL2CPP_METHOD_ATTR void Level_Init_m3675134387 (Level_t2237665516 * __this, const RuntimeMethod* method);
// System.Void Player::Init()
extern "C" IL2CPP_METHOD_ATTR void Player_Init_m3348666991 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.String System.Int32::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Int32_ToString_m141394615 (int32_t* __this, const RuntimeMethod* method);
// System.Single Player::get_stress()
extern "C" IL2CPP_METHOD_ATTR float Player_get_stress_m4229801089 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.Void Level::Deactivate()
extern "C" IL2CPP_METHOD_ATTR void Level_Deactivate_m271886484 (Level_t2237665516 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<DeathMenu>()
inline DeathMenu_t1753622066 * GameObject_GetComponent_TisDeathMenu_t1753622066_m3748280843 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  DeathMenu_t1753622066 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::.ctor()
inline void List_1__ctor_m1424466557 (List_1_t2585711361 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Tilemaps.Tilemap>()
inline Tilemap_t1578933799 * GameObject_GetComponent_TisTilemap_t1578933799_m3651117485 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Tilemap_t1578933799 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void Level::Generate(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Level_Generate_m932420581 (Level_t2237665516 * __this, int32_t ___size0, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m2622589598 (Action_1_t1286104214 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t1286104214 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::ForEach(System.Action`1<!0>)
inline void List_1_ForEach_m2842337338 (List_1_t2585711361 * __this, Action_1_t1286104214 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2585711361 *, Action_1_t1286104214 *, const RuntimeMethod*))List_1_ForEach_m3737504377_gshared)(__this, p0, method);
}
// System.Int32 System.Array::GetLength(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Array_GetLength_m2178203778 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Vector2Int__ctor_m3872920888 (Vector2Int_t3469998543 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_x()
extern "C" IL2CPP_METHOD_ATTR int32_t Vector2Int_get_x_m64542184 (Vector2Int_t3469998543 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Vector2Int::get_y()
extern "C" IL2CPP_METHOD_ATTR int32_t Vector2Int_get_y_m64542185 (Vector2Int_t3469998543 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2Int Level::find(Level/TileType[0...,0...],Level/TileType)
extern "C" IL2CPP_METHOD_ATTR Vector2Int_t3469998543  Level_find_m3383925018 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___type1, const RuntimeMethod* method);
// System.Void Level::fill(Level/TileType[0...,0...],System.Int32,System.Int32,System.Int32,System.Int32,Level/TileType)
extern "C" IL2CPP_METHOD_ATTR void Level_fill_m1857042071 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, int32_t ___type5, const RuntimeMethod* method);
// UnityEngine.Vector2Int UnityEngine.Vector2Int::op_Addition(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern "C" IL2CPP_METHOD_ATTR Vector2Int_t3469998543  Vector2Int_op_Addition_m1244329832 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, Vector2Int_t3469998543  p1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m3353183577 (Vector3_t3722313464 * __this, float p0, float p1, float p2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_t1113636619 * Object_Instantiate_TisGameObject_t1113636619_m3006960551 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * p0, Vector3_t3722313464  p1, Quaternion_t2301928331  p2, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (RuntimeObject * /* static, unused */, GameObject_t1113636619 *, Vector3_t3722313464 , Quaternion_t2301928331 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m1135049463_gshared)(__this /* static, unused */, p0, p1, p2, method);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" IL2CPP_METHOD_ATTR void Object_set_name_m291480324 (Object_t631007953 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0)
inline void List_1_Add_m2765963565 (List_1_t2585711361 * __this, GameObject_t1113636619 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2585711361 *, GameObject_t1113636619 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void Level::cutChannel(Level/TileType[0...,0...],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Level_cutChannel_m3357319375 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___radius1, int32_t ___length2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t Mathf_Abs_m2460432655 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Tilemaps.Tilemap::ClearAllTiles()
extern "C" IL2CPP_METHOD_ATTR void Tilemap_ClearAllTiles_m1382480930 (Tilemap_t1578933799 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Vector3Int__ctor_m2885707673 (Vector3Int_t741115188 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Tilemaps.Tilemap::SetTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.TileBase)
extern "C" IL2CPP_METHOD_ATTR void Tilemap_SetTile_m1236447073 (Tilemap_t1578933799 * __this, Vector3Int_t741115188  p0, TileBase_t3985733243 * p1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Resources_Load_m3880010804 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector3__ctor_m1719387948 (Vector3_t3722313464 * __this, float p0, float p1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method);
// UnityEngine.GameObject Level::instantiate(UnityEngine.GameObject,UnityEngine.Vector2,System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Level_instantiate_m2506382120 (Level_t2237665516 * __this, GameObject_t1113636619 * ___type0, Vector2_t2156229523  ___position1, String_t* ___name2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Water>()
inline Water_t1083516957 * GameObject_GetComponent_TisWater_t1083516957_m400380985 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Water_t1083516957 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void Water/Activator::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Activator__ctor_m1355085645 (Activator_t2573376075 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void Water::SetActivator(Water/Activator)
extern "C" IL2CPP_METHOD_ATTR void Water_SetActivator_m3781101325 (Water_t1083516957 * __this, Activator_t2573376075 * ___activator0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2Int::op_Implicit(UnityEngine.Vector2Int)
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2Int_op_Implicit_m1284226264 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_Destroy_m565254235 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void MainMenu::set_menuDuration(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainMenu_set_menuDuration_m1906417042 (MainMenu_t3798339593 * __this, float ___value0, const RuntimeMethod* method);
// System.Void MainMenu::set_blinkTime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainMenu_set_blinkTime_m377316010 (MainMenu_t3798339593 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButton(System.String)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetButton_m2064261504 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Single MainMenu::get_blinkTime()
extern "C" IL2CPP_METHOD_ATTR float MainMenu_get_blinkTime_m416367648 (MainMenu_t3798339593 * __this, const RuntimeMethod* method);
// System.Single MainMenu::get_menuDuration()
extern "C" IL2CPP_METHOD_ATTR float MainMenu_get_menuDuration_m1640785792 (MainMenu_t3798339593 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<GameScreen>()
inline GameScreen_t1800529819 * GameObject_GetComponent_TisGameScreen_t1800529819_m2973029269 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  GameScreen_t1800529819 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Max_m3146388979 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Material_SetFloat_m3226510453 (Material_t340375123 * __this, String_t* p0, float p1, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material)
extern "C" IL2CPP_METHOD_ATTR void Graphics_Blit_m890955694 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * p0, RenderTexture_t2108887433 * p1, Material_t340375123 * p2, const RuntimeMethod* method);
// System.Void Monster::Init()
extern "C" IL2CPP_METHOD_ATTR void Monster_Init_m664643989 (Monster_t1049719775 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_t939494601 * Component_GetComponent_TisRigidbody2D_t939494601_m1531613439 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_t939494601 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.CircleCollider2D>()
inline CircleCollider2D_t662546754 * Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  CircleCollider2D_t662546754 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_m1033527003_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t3935305588 * Component_GetComponent_TisAudioSource_t3935305588_m1977431131 (Component_t1923634451 * __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t3935305588 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method);
}
// System.Void CollisionEventer/TriggerListener::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TriggerListener__ctor_m813156148 (TriggerListener_t1115224763 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void CollisionEventer::OnTriggerEnter2D(UnityEngine.GameObject,CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerEnter2D_m1969312576 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, TriggerListener_t1115224763 * ___listener1, const RuntimeMethod* method);
// System.Void CollisionEventer::OnTriggerExit2D(UnityEngine.GameObject,CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerExit2D_m3161418375 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, TriggerListener_t1115224763 * ___listener1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Vector3_op_Subtraction_m3073674971 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Transform_get_rotation_m3502953881 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_get_eulerAngles_m3425202016 (Quaternion_t2301928331 * __this, const RuntimeMethod* method);
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR RaycastHit2DU5BU5D_t4286651560* Physics2D_RaycastAll_m3534418674 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Void System.Comparison`1<UnityEngine.RaycastHit2D>::.ctor(System.Object,System.IntPtr)
inline void Comparison_1__ctor_m1180004629 (Comparison_1_t2054513168 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Comparison_1_t2054513168 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Comparison_1__ctor_m1180004629_gshared)(__this, p0, p1, method);
}
// System.Void System.Array::Sort<UnityEngine.RaycastHit2D>(!!0[],System.Comparison`1<!!0>)
inline void Array_Sort_TisRaycastHit2D_t2279581989_m3365424645 (RuntimeObject * __this /* static, unused */, RaycastHit2DU5BU5D_t4286651560* p0, Comparison_1_t2054513168 * p1, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject * /* static, unused */, RaycastHit2DU5BU5D_t4286651560*, Comparison_1_t2054513168 *, const RuntimeMethod*))Array_Sort_TisRaycastHit2D_t2279581989_m3365424645_gshared)(__this /* static, unused */, p0, p1, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * RaycastHit2D_get_transform_m2048267409 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// System.String UnityEngine.GameObject::get_tag()
extern "C" IL2CPP_METHOD_ATTR String_t* GameObject_get_tag_m3951609671 (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m2898400508 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  p0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rigidbody2D_set_angularVelocity_m2791812150 (Rigidbody2D_t939494601 * __this, float p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_t179987942  List_1_GetEnumerator_m1750140655 (List_1_t2585711361 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t179987942  (*) (List_1_t2585711361 *, const RuntimeMethod*))List_1_GetEnumerator_m816315209_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_t1113636619 * Enumerator_get_Current_m4179928398 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_t1113636619 * (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_get_Current_m337713592_gshared)(__this, method);
}
// System.Boolean Monster::facing(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool Monster_facing_m1121058899 (Monster_t1049719775 * __this, GameObject_t1113636619 * ___obj0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_m4286844348 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_MoveNext_m2142368520_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m1341201278 (Enumerator_t179987942 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t179987942 *, const RuntimeMethod*))Enumerator_Dispose_m3007748546_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR Quaternion_t2301928331  Quaternion_Euler_m3049309462 (RuntimeObject * __this /* static, unused */, float p0, float p1, float p2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C" IL2CPP_METHOD_ATTR void Transform_set_rotation_m3524318132 (Transform_t3600365921 * __this, Quaternion_t2301928331  p0, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::Play()
extern "C" IL2CPP_METHOD_ATTR void AudioSource_Play_m48294159 (AudioSource_t3935305588 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3574996620 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C" IL2CPP_METHOD_ATTR Vector3_t3722313464  Quaternion_op_Multiply_m2607404835 (RuntimeObject * __this /* static, unused */, Quaternion_t2301928331  p0, Vector3_t3722313464  p1, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.Array System.Enum::GetValues(System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeArray * Enum_GetValues_m4192343468 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method);
// System.Int32 System.Array::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t Array_get_Length_m21610649 (RuntimeArray * __this, const RuntimeMethod* method);
// System.Object System.Array::GetValue(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Array_GetValue_m2528546681 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Random_Range_m2202990745 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.GameObject>::Remove(!0)
inline bool List_1_Remove_m4063777476 (List_1_t2585711361 * __this, GameObject_t1113636619 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t2585711361 *, GameObject_t1113636619 *, const RuntimeMethod*))List_1_Remove_m2390619627_gshared)(__this, p0, method);
}
// System.Single UnityEngine.RaycastHit2D::get_distance()
extern "C" IL2CPP_METHOD_ATTR float RaycastHit2D_get_distance_m382898860 (RaycastHit2D_t2279581989 * __this, const RuntimeMethod* method);
// System.Void Player::set_stress(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Player_set_stress_m4170066995 (Player_t3266647312 * __this, float ___value0, const RuntimeMethod* method);
// System.Void Player::set_blink(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Player_set_blink_m3504210868 (Player_t3266647312 * __this, float ___value0, const RuntimeMethod* method);
// System.Void Player::set_poolsDiscovered(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Player_set_poolsDiscovered_m3631500643 (Player_t3266647312 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void CollisionEventer::ClearTriggerEnter2D(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerEnter2D_m671181426 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, const RuntimeMethod* method);
// System.Void CollisionEventer::ClearTriggerExit2D(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerExit2D_m42893453 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, const RuntimeMethod* method);
// System.Void Player::PlayerInput()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerInput_m1067569136 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.Void Player::PlayerLogic()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerLogic_m3769131123 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.Void Player::PlayerEffects()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerEffects_m1429873540 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" IL2CPP_METHOD_ATTR float Input_GetAxisRaw_m2316819917 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Single Player::get_blink()
extern "C" IL2CPP_METHOD_ATTR float Player_get_blink_m816255482 (Player_t3266647312 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR float Vector2_Distance_m3048868881 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR float Mathf_Min_m1073399594 (RuntimeObject * __this /* static, unused */, float p0, float p1, const RuntimeMethod* method);
// System.Void Water::Activate()
extern "C" IL2CPP_METHOD_ATTR void Water_Activate_m1474361647 (Water_t1083516957 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C" IL2CPP_METHOD_ATTR void AudioSource_set_volume_m1273312851 (AudioSource_t3935305588 * __this, float p0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_get_parent_m835071599 (Transform_t3600365921 * __this, const RuntimeMethod* method);
// System.Boolean Water::get_Discovered()
extern "C" IL2CPP_METHOD_ATTR bool Water_get_Discovered_m4177957938 (Water_t1083516957 * __this, const RuntimeMethod* method);
// System.Void Water::Discover()
extern "C" IL2CPP_METHOD_ATTR void Water_Discover_m2700990817 (Water_t1083516957 * __this, const RuntimeMethod* method);
// System.Void Water::set_Discovered(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Water_set_Discovered_m187261137 (Water_t1083516957 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR Transform_t3600365921 * Transform_Find_m1729760951 (Transform_t3600365921 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider2D>()
inline BoxCollider2D_t3581341831 * GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  BoxCollider2D_t3581341831 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void UnityEngine.Physics2D::IgnoreCollision(UnityEngine.Collider2D,UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Physics2D_IgnoreCollision_m4213295628 (RuntimeObject * __this /* static, unused */, Collider2D_t2806799626 * p0, Collider2D_t2806799626 * p1, const RuntimeMethod* method);
// System.Void Water::set_Active(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Water_set_Active_m918479718 (Water_t1083516957 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void Water/Activator::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Activator_Invoke_m60796017 (Activator_t2573376075 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CollisionEventer::.ctor()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer__ctor_m2507749900 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer__ctor_m2507749900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2587299505 * L_0 = (List_1_t2587299505 *)il2cpp_codegen_object_new(List_1_t2587299505_il2cpp_TypeInfo_var);
		List_1__ctor_m3280379170(L_0, /*hidden argument*/List_1__ctor_m3280379170_RuntimeMethod_var);
		__this->set_enterListeners_4(L_0);
		List_1_t2587299505 * L_1 = (List_1_t2587299505 *)il2cpp_codegen_object_new(List_1_t2587299505_il2cpp_TypeInfo_var);
		List_1__ctor_m3280379170(L_1, /*hidden argument*/List_1__ctor_m3280379170_RuntimeMethod_var);
		__this->set_exitListeners_5(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollisionEventer::Start()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_Start_m850317074 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void CollisionEventer::OnTriggerEnter2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerEnter2D_m1653861538 (CollisionEventer_t1111808458 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerEnter2D_m1653861538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TriggerListener_t1115224763 * V_0 = NULL;
	Enumerator_t181576086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2587299505 * L_0 = __this->get_enterListeners_4();
		Enumerator_t181576086  L_1 = List_1_GetEnumerator_m3837348380(L_0, /*hidden argument*/List_1_GetEnumerator_m3837348380_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_0011:
		{
			TriggerListener_t1115224763 * L_2 = Enumerator_get_Current_m959559447((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m959559447_RuntimeMethod_var);
			V_0 = L_2;
			TriggerListener_t1115224763 * L_3 = V_0;
			Collider2D_t2806799626 * L_4 = ___other0;
			TriggerListener_Invoke_m2431345097(L_3, L_4, /*hidden argument*/NULL);
		}

IL_0020:
		{
			bool L_5 = Enumerator_MoveNext_m1996258908((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m1996258908_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m765708873((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m765708873_RuntimeMethod_var);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void CollisionEventer::OnTriggerExit2D(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerExit2D_m3144928147 (CollisionEventer_t1111808458 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerExit2D_m3144928147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TriggerListener_t1115224763 * V_0 = NULL;
	Enumerator_t181576086  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2587299505 * L_0 = __this->get_exitListeners_5();
		Enumerator_t181576086  L_1 = List_1_GetEnumerator_m3837348380(L_0, /*hidden argument*/List_1_GetEnumerator_m3837348380_RuntimeMethod_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0020;
		}

IL_0011:
		{
			TriggerListener_t1115224763 * L_2 = Enumerator_get_Current_m959559447((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m959559447_RuntimeMethod_var);
			V_0 = L_2;
			TriggerListener_t1115224763 * L_3 = V_0;
			Collider2D_t2806799626 * L_4 = ___other0;
			TriggerListener_Invoke_m2431345097(L_3, L_4, /*hidden argument*/NULL);
		}

IL_0020:
		{
			bool L_5 = Enumerator_MoveNext_m1996258908((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m1996258908_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_002c:
		{
			IL2CPP_LEAVE(0x3F, FINALLY_0031);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0031;
	}

FINALLY_0031:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m765708873((Enumerator_t181576086 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m765708873_RuntimeMethod_var);
		IL2CPP_END_FINALLY(49)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(49)
	{
		IL2CPP_JUMP_TBL(0x3F, IL_003f)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_003f:
	{
		return;
	}
}
// System.Void CollisionEventer::OnTriggerEnter2D(CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerEnter2D_m3032026089 (CollisionEventer_t1111808458 * __this, TriggerListener_t1115224763 * ___listener0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerEnter2D_m3032026089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2587299505 * L_0 = __this->get_enterListeners_4();
		TriggerListener_t1115224763 * L_1 = ___listener0;
		List_1_Add_m1077912546(L_0, L_1, /*hidden argument*/List_1_Add_m1077912546_RuntimeMethod_var);
		return;
	}
}
// System.Void CollisionEventer::OnTriggerExit2D(CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerExit2D_m61367443 (CollisionEventer_t1111808458 * __this, TriggerListener_t1115224763 * ___listener0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerExit2D_m61367443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2587299505 * L_0 = __this->get_exitListeners_5();
		TriggerListener_t1115224763 * L_1 = ___listener0;
		List_1_Add_m1077912546(L_0, L_1, /*hidden argument*/List_1_Add_m1077912546_RuntimeMethod_var);
		return;
	}
}
// System.Void CollisionEventer::ClearTriggerEnter2D()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerEnter2D_m448421859 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_ClearTriggerEnter2D_m448421859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2587299505 * L_0 = __this->get_enterListeners_4();
		List_1_Clear_m3309761785(L_0, /*hidden argument*/List_1_Clear_m3309761785_RuntimeMethod_var);
		return;
	}
}
// System.Void CollisionEventer::ClearTriggerExit2D()
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerExit2D_m704284422 (CollisionEventer_t1111808458 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_ClearTriggerExit2D_m704284422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2587299505 * L_0 = __this->get_exitListeners_5();
		List_1_Clear_m3309761785(L_0, /*hidden argument*/List_1_Clear_m3309761785_RuntimeMethod_var);
		return;
	}
}
// System.Void CollisionEventer::OnTriggerEnter2D(UnityEngine.GameObject,CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerEnter2D_m1969312576 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, TriggerListener_t1115224763 * ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerEnter2D_m1969312576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEventer_t1111808458 * V_0 = NULL;
	CollisionEventer_t1111808458 * G_B2_0 = NULL;
	CollisionEventer_t1111808458 * G_B1_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___collider0;
		CollisionEventer_t1111808458 * L_1 = GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016(L_0, /*hidden argument*/GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016_RuntimeMethod_var);
		CollisionEventer_t1111808458 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0013;
		}
	}
	{
		GameObject_t1113636619 * L_3 = ___collider0;
		CollisionEventer_t1111808458 * L_4 = GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471(L_3, /*hidden argument*/GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471_RuntimeMethod_var);
		G_B2_0 = L_4;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		CollisionEventer_t1111808458 * L_5 = V_0;
		TriggerListener_t1115224763 * L_6 = ___listener1;
		CollisionEventer_OnTriggerEnter2D_m3032026089(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollisionEventer::OnTriggerExit2D(UnityEngine.GameObject,CollisionEventer/TriggerListener)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_OnTriggerExit2D_m3161418375 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, TriggerListener_t1115224763 * ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_OnTriggerExit2D_m3161418375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEventer_t1111808458 * V_0 = NULL;
	CollisionEventer_t1111808458 * G_B2_0 = NULL;
	CollisionEventer_t1111808458 * G_B1_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___collider0;
		CollisionEventer_t1111808458 * L_1 = GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016(L_0, /*hidden argument*/GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016_RuntimeMethod_var);
		CollisionEventer_t1111808458 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0013;
		}
	}
	{
		GameObject_t1113636619 * L_3 = ___collider0;
		CollisionEventer_t1111808458 * L_4 = GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471(L_3, /*hidden argument*/GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471_RuntimeMethod_var);
		G_B2_0 = L_4;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		CollisionEventer_t1111808458 * L_5 = V_0;
		TriggerListener_t1115224763 * L_6 = ___listener1;
		CollisionEventer_OnTriggerExit2D_m61367443(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollisionEventer::ClearTriggerEnter2D(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerEnter2D_m671181426 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_ClearTriggerEnter2D_m671181426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEventer_t1111808458 * V_0 = NULL;
	CollisionEventer_t1111808458 * G_B2_0 = NULL;
	CollisionEventer_t1111808458 * G_B1_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___collider0;
		CollisionEventer_t1111808458 * L_1 = GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016(L_0, /*hidden argument*/GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016_RuntimeMethod_var);
		CollisionEventer_t1111808458 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0013;
		}
	}
	{
		GameObject_t1113636619 * L_3 = ___collider0;
		CollisionEventer_t1111808458 * L_4 = GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471(L_3, /*hidden argument*/GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471_RuntimeMethod_var);
		G_B2_0 = L_4;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		CollisionEventer_t1111808458 * L_5 = V_0;
		CollisionEventer_ClearTriggerEnter2D_m448421859(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CollisionEventer::ClearTriggerExit2D(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void CollisionEventer_ClearTriggerExit2D_m42893453 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CollisionEventer_ClearTriggerExit2D_m42893453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CollisionEventer_t1111808458 * V_0 = NULL;
	CollisionEventer_t1111808458 * G_B2_0 = NULL;
	CollisionEventer_t1111808458 * G_B1_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___collider0;
		CollisionEventer_t1111808458 * L_1 = GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016(L_0, /*hidden argument*/GameObject_GetComponent_TisCollisionEventer_t1111808458_m1407471016_RuntimeMethod_var);
		CollisionEventer_t1111808458 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0013;
		}
	}
	{
		GameObject_t1113636619 * L_3 = ___collider0;
		CollisionEventer_t1111808458 * L_4 = GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471(L_3, /*hidden argument*/GameObject_AddComponent_TisCollisionEventer_t1111808458_m769148471_RuntimeMethod_var);
		G_B2_0 = L_4;
	}

IL_0013:
	{
		V_0 = G_B2_0;
		CollisionEventer_t1111808458 * L_5 = V_0;
		CollisionEventer_ClearTriggerExit2D_m704284422(L_5, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CollisionEventer/TriggerListener::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void TriggerListener__ctor_m813156148 (TriggerListener_t1115224763 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void CollisionEventer/TriggerListener::Invoke(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void TriggerListener_Invoke_m2431345097 (TriggerListener_t1115224763 * __this, Collider2D_t2806799626 * ___other0, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, Collider2D_t2806799626 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, ___other0, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collider2D_t2806799626 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___other0, targetMethod);
					}
				}
			}
			else
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
							else
								GenericVirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___other0);
							else
								VirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___other0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, Collider2D_t2806799626 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, ___other0, targetMethod);
					}
				}
				else
				{
					// open
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, ___other0);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, ___other0);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___other0);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___other0);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (Collider2D_t2806799626 *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(___other0, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, Collider2D_t2806799626 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, ___other0, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, Collider2D_t2806799626 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, ___other0, targetMethod);
				}
			}
		}
		else
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 1)
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
						else
							GenericVirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(targetMethod, targetThis, ___other0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___other0);
						else
							VirtActionInvoker1< Collider2D_t2806799626 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___other0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, Collider2D_t2806799626 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___other0, targetMethod);
				}
			}
			else
			{
				// open
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, ___other0);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, ___other0);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___other0);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___other0);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (Collider2D_t2806799626 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___other0, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult CollisionEventer/TriggerListener::BeginInvoke(UnityEngine.Collider2D,System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TriggerListener_BeginInvoke_m2985176655 (TriggerListener_t1115224763 * __this, Collider2D_t2806799626 * ___other0, AsyncCallback_t3962456242 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___other0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void CollisionEventer/TriggerListener::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void TriggerListener_EndInvoke_m2971450099 (TriggerListener_t1115224763 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DeathMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DeathMenu__ctor_m4144235032 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DeathMenu::get_menuDuration()
extern "C" IL2CPP_METHOD_ATTR float DeathMenu_get_menuDuration_m1418593762 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CmenuDurationU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void DeathMenu::set_menuDuration(System.Single)
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_set_menuDuration_m761959042 (DeathMenu_t1753622066 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CmenuDurationU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Single DeathMenu::get_time()
extern "C" IL2CPP_METHOD_ATTR float DeathMenu_get_time_m239362606 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CtimeU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void DeathMenu::set_time(System.Single)
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_set_time_m3325743434 (DeathMenu_t1753622066 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CtimeU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void DeathMenu::Start()
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_Start_m869472114 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	{
		DeathMenu_Init_m3830644225(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DeathMenu::Init()
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_Init_m3830644225 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeathMenu_Init_m3830644225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1512031223, /*hidden argument*/NULL);
		Text_t1901882714 * L_1 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		__this->set_score_8(L_1);
		GameObject_t1113636619 * L_2 = __this->get_player_6();
		Player_t3266647312 * L_3 = GameObject_GetComponent_TisPlayer_t3266647312_m4068145281(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayer_t3266647312_m4068145281_RuntimeMethod_var);
		__this->set_playerComponent_7(L_3);
		DeathMenu_set_menuDuration_m761959042(__this, (4.0f), /*hidden argument*/NULL);
		DeathMenu_set_time_m3325743434(__this, (0.0f), /*hidden argument*/NULL);
		Text_t1901882714 * L_4 = __this->get_score_8();
		Player_t3266647312 * L_5 = __this->get_playerComponent_7();
		int32_t L_6 = Player_get_poolsDiscovered_m1228037983(L_5, /*hidden argument*/NULL);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		String_t* L_9 = String_Concat_m1715369213(NULL /*static, unused*/, _stringLiteral1652603074, L_8, _stringLiteral3513529601, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_9);
		return;
	}
}
// System.Void DeathMenu::Update()
extern "C" IL2CPP_METHOD_ATTR void DeathMenu_Update_m993778007 (DeathMenu_t1753622066 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DeathMenu_Update_m993778007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = DeathMenu_get_time_m239362606(__this, /*hidden argument*/NULL);
		float L_1 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		DeathMenu_set_time_m3325743434(__this, ((float)il2cpp_codegen_add((float)L_0, (float)L_1)), /*hidden argument*/NULL);
		float L_2 = DeathMenu_get_time_m239362606(__this, /*hidden argument*/NULL);
		float L_3 = DeathMenu_get_menuDuration_m1418593762(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_004b;
		}
	}
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_4, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_main_5();
		GameObject_SetActive_m796801857(L_5, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = __this->get_main_5();
		MainMenu_t3798339593 * L_7 = GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124(L_6, /*hidden argument*/GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124_RuntimeMethod_var);
		MainMenu_Init_m4196376439(L_7, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GameScreen::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GameScreen__ctor_m2718863737 (GameScreen_t1800529819 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameScreen::Start()
extern "C" IL2CPP_METHOD_ATTR void GameScreen_Start_m2902530691 (GameScreen_t1800529819 * __this, const RuntimeMethod* method)
{
	{
		GameScreen_Init_m2409608521(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameScreen::Init()
extern "C" IL2CPP_METHOD_ATTR void GameScreen_Init_m2409608521 (GameScreen_t1800529819 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_Init_m2409608521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral3587759650, /*hidden argument*/NULL);
		Text_t1901882714 * L_1 = GameObject_GetComponent_TisText_t1901882714_m2114913816(L_0, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m2114913816_RuntimeMethod_var);
		__this->set_score_9(L_1);
		GameObject_t1113636619 * L_2 = __this->get_level_7();
		Level_t2237665516 * L_3 = GameObject_GetComponent_TisLevel_t2237665516_m1206308685(L_2, /*hidden argument*/GameObject_GetComponent_TisLevel_t2237665516_m1206308685_RuntimeMethod_var);
		__this->set_levelComponent_8(L_3);
		Level_t2237665516 * L_4 = __this->get_levelComponent_8();
		Level_Init_m3675134387(L_4, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_5 = __this->get_player_4();
		Player_t3266647312 * L_6 = GameObject_GetComponent_TisPlayer_t3266647312_m4068145281(L_5, /*hidden argument*/GameObject_GetComponent_TisPlayer_t3266647312_m4068145281_RuntimeMethod_var);
		__this->set_playerComponent_5(L_6);
		Player_t3266647312 * L_7 = __this->get_playerComponent_5();
		Player_Init_m3348666991(L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameScreen::Update()
extern "C" IL2CPP_METHOD_ATTR void GameScreen_Update_m3339885975 (GameScreen_t1800529819 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameScreen_Update_m3339885975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Text_t1901882714 * L_0 = __this->get_score_9();
		Player_t3266647312 * L_1 = __this->get_playerComponent_5();
		int32_t L_2 = Player_get_poolsDiscovered_m1228037983(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		Player_t3266647312 * L_4 = __this->get_playerComponent_5();
		float L_5 = Player_get_stress_m4229801089(L_4, /*hidden argument*/NULL);
		if ((!(((float)L_5) >= ((float)(1.0f)))))
		{
			goto IL_006c;
		}
	}
	{
		Level_t2237665516 * L_6 = __this->get_levelComponent_8();
		Level_Deactivate_m271886484(L_6, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_deathScreen_6();
		GameObject_SetActive_m796801857(L_8, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_9 = __this->get_deathScreen_6();
		DeathMenu_t1753622066 * L_10 = GameObject_GetComponent_TisDeathMenu_t1753622066_m3748280843(L_9, /*hidden argument*/GameObject_GetComponent_TisDeathMenu_t1753622066_m3748280843_RuntimeMethod_var);
		DeathMenu_Init_m3830644225(L_10, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Level::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Level__ctor_m38370482 (Level_t2237665516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level__ctor_m38370482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_size_10(((int32_t)200));
		List_1_t2585711361 * L_0 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m1424466557(L_0, /*hidden argument*/List_1__ctor_m1424466557_RuntimeMethod_var);
		__this->set_entities_11(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::Start()
extern "C" IL2CPP_METHOD_ATTR void Level_Start_m2546324142 (Level_t2237665516 * __this, const RuntimeMethod* method)
{
	{
		Level_Init_m3675134387(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::Init()
extern "C" IL2CPP_METHOD_ATTR void Level_Init_m3675134387 (Level_t2237665516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_Init_m3675134387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Level_Deactivate_m271886484(__this, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		Player_t3266647312 * L_1 = GameObject_GetComponent_TisPlayer_t3266647312_m4068145281(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayer_t3266647312_m4068145281_RuntimeMethod_var);
		__this->set_player_9(L_1);
		GameObject_t1113636619 * L_2 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral1189462381, /*hidden argument*/NULL);
		Tilemap_t1578933799 * L_3 = GameObject_GetComponent_TisTilemap_t1578933799_m3651117485(L_2, /*hidden argument*/GameObject_GetComponent_TisTilemap_t1578933799_m3651117485_RuntimeMethod_var);
		__this->set_floorMap_7(L_3);
		GameObject_t1113636619 * L_4 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral7955884, /*hidden argument*/NULL);
		Tilemap_t1578933799 * L_5 = GameObject_GetComponent_TisTilemap_t1578933799_m3651117485(L_4, /*hidden argument*/GameObject_GetComponent_TisTilemap_t1578933799_m3651117485_RuntimeMethod_var);
		__this->set_wallMap_8(L_5);
		int32_t L_6 = __this->get_size_10();
		Level_Generate_m932420581(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::Deactivate()
extern "C" IL2CPP_METHOD_ATTR void Level_Deactivate_m271886484 (Level_t2237665516 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_Deactivate_m271886484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2585711361 * G_B2_0 = NULL;
	List_1_t2585711361 * G_B1_0 = NULL;
	{
		List_1_t2585711361 * L_0 = __this->get_entities_11();
		Action_1_t1286104214 * L_1 = ((Level_t2237665516_StaticFields*)il2cpp_codegen_static_fields_for(Level_t2237665516_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_12();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		intptr_t L_2 = (intptr_t)Level_U3CDeactivateU3Em__0_m4082416521_RuntimeMethod_var;
		Action_1_t1286104214 * L_3 = (Action_1_t1286104214 *)il2cpp_codegen_object_new(Action_1_t1286104214_il2cpp_TypeInfo_var);
		Action_1__ctor_m2622589598(L_3, NULL, L_2, /*hidden argument*/Action_1__ctor_m2622589598_RuntimeMethod_var);
		((Level_t2237665516_StaticFields*)il2cpp_codegen_static_fields_for(Level_t2237665516_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_12(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Action_1_t1286104214 * L_4 = ((Level_t2237665516_StaticFields*)il2cpp_codegen_static_fields_for(Level_t2237665516_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_12();
		List_1_ForEach_m2842337338(G_B2_0, L_4, /*hidden argument*/List_1_ForEach_m2842337338_RuntimeMethod_var);
		List_1_t2585711361 * L_5 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m1424466557(L_5, /*hidden argument*/List_1__ctor_m1424466557_RuntimeMethod_var);
		__this->set_entities_11(L_5);
		return;
	}
}
// System.Void Level::fill(Level/TileType[0...,0...],System.Int32,System.Int32,System.Int32,System.Int32,Level/TileType)
extern "C" IL2CPP_METHOD_ATTR void Level_fill_m1857042071 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___x1, int32_t ___y2, int32_t ___width3, int32_t ___height4, int32_t ___type5, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = ___x1;
		int32_t L_1 = ___width3;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
		int32_t L_2 = ___y2;
		int32_t L_3 = ___height4;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_2, (int32_t)L_3));
		int32_t L_4 = ___x1;
		V_2 = L_4;
		goto IL_0031;
	}

IL_0011:
	{
		int32_t L_5 = ___y2;
		V_3 = L_5;
		goto IL_0026;
	}

IL_0018:
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_6 = ___map0;
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		int32_t L_9 = ___type5;
		(L_6)->SetAtUnchecked(L_7, L_8, L_9);
		int32_t L_10 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_11 = V_3;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0031:
	{
		int32_t L_14 = V_2;
		int32_t L_15 = V_0;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
// UnityEngine.Vector2Int Level::find(Level/TileType[0...,0...],Level/TileType)
extern "C" IL2CPP_METHOD_ATTR Vector2Int_t3469998543  Level_find_m3383925018 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___type1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Vector2Int_t3469998543  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_0 = ___map0;
		int32_t L_1 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_0008:
	{
		int32_t L_2 = V_0;
		int32_t L_3 = Random_Range_m4054026115(NULL /*static, unused*/, 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		int32_t L_5 = Random_Range_m4054026115(NULL /*static, unused*/, 1, ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)), /*hidden argument*/NULL);
		Vector2Int__ctor_m3872920888((Vector2Int_t3469998543 *)(&V_1), L_3, L_5, /*hidden argument*/NULL);
		TileTypeU5B0___U2C0___U5D_t2292257856* L_6 = ___map0;
		int32_t L_7 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_1), /*hidden argument*/NULL);
		int32_t L_8 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_1), /*hidden argument*/NULL);
		int32_t L_9 = (L_6)->GetAtUnchecked(L_7, L_8);
		int32_t L_10 = ___type1;
		if ((!(((uint32_t)L_9) == ((uint32_t)L_10))))
		{
			goto IL_0008;
		}
	}
	{
		Vector2Int_t3469998543  L_11 = V_1;
		return L_11;
	}
}
// System.Void Level::cutChannel(Level/TileType[0...,0...],System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Level_cutChannel_m3357319375 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___radius1, int32_t ___length2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_cutChannel_m3357319375_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Vector2Int_t3469998543  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Vector2Int_t3469998543  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2Int_t3469998543  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t V_5 = 0;
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_0 = ___map0;
		int32_t L_1 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		TileTypeU5B0___U2C0___U5D_t2292257856* L_2 = ___map0;
		Vector2Int_t3469998543  L_3 = Level_find_m3383925018(__this, L_2, 1, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Random_Range_m4054026115(NULL /*static, unused*/, 2, 5, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_2;
		int32_t L_6 = V_2;
		int32_t L_7 = Random_Range_m4054026115(NULL /*static, unused*/, ((-L_5)), L_6, /*hidden argument*/NULL);
		int32_t L_8 = V_2;
		int32_t L_9 = V_2;
		int32_t L_10 = Random_Range_m4054026115(NULL /*static, unused*/, ((-L_8)), L_9, /*hidden argument*/NULL);
		Vector2Int__ctor_m3872920888((Vector2Int_t3469998543 *)(&V_3), L_7, L_10, /*hidden argument*/NULL);
		int32_t L_11 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_1), /*hidden argument*/NULL);
		int32_t L_12 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_1), /*hidden argument*/NULL);
		Vector2Int__ctor_m3872920888((Vector2Int_t3469998543 *)(&V_4), L_11, L_12, /*hidden argument*/NULL);
		V_5 = 0;
		goto IL_00ba;
	}

IL_004d:
	{
		int32_t L_13 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_14 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)L_14))) < ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_15 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_16 = V_2;
		int32_t L_17 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)L_16))) > ((int32_t)L_17)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_18 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		if ((((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)L_19))) < ((int32_t)0)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_20 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_21 = V_2;
		int32_t L_22 = V_0;
		if ((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_21))) <= ((int32_t)L_22)))
		{
			goto IL_008a;
		}
	}

IL_0089:
	{
		return;
	}

IL_008a:
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_23 = ___map0;
		int32_t L_24 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_25 = V_2;
		int32_t L_26 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_4), /*hidden argument*/NULL);
		int32_t L_27 = V_2;
		int32_t L_28 = V_2;
		int32_t L_29 = V_2;
		Level_fill_m1857042071(__this, L_23, ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25)), ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)L_27)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_28, (int32_t)2)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_29, (int32_t)2)), 1, /*hidden argument*/NULL);
		Vector2Int_t3469998543  L_30 = V_4;
		Vector2Int_t3469998543  L_31 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		Vector2Int_t3469998543  L_32 = Vector2Int_op_Addition_m1244329832(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		int32_t L_33 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00ba:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = ___length2;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_004d;
		}
	}
	{
		return;
	}
}
// UnityEngine.GameObject Level::instantiate(UnityEngine.GameObject,UnityEngine.Vector2,System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Level_instantiate_m2506382120 (Level_t2237665516 * __this, GameObject_t1113636619 * ___type0, Vector2_t2156229523  ___position1, String_t* ___name2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_instantiate_m2506382120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t1113636619 * V_1 = NULL;
	Quaternion_t2301928331  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = (&___position1)->get_x_0();
		float L_1 = (&___position1)->get_y_1();
		Vector3__ctor_m3353183577((Vector3_t3722313464 *)(&V_0), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_2 = ___type0;
		Vector2_t2156229523  L_3 = ___position1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_4 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_2), sizeof(Quaternion_t2301928331 ));
		Quaternion_t2301928331  L_5 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		GameObject_t1113636619 * L_6 = Object_Instantiate_TisGameObject_t1113636619_m3006960551(NULL /*static, unused*/, L_2, L_4, L_5, /*hidden argument*/Object_Instantiate_TisGameObject_t1113636619_m3006960551_RuntimeMethod_var);
		V_1 = L_6;
		GameObject_t1113636619 * L_7 = V_1;
		String_t* L_8 = ___name2;
		Object_set_name_m291480324(L_7, L_8, /*hidden argument*/NULL);
		List_1_t2585711361 * L_9 = __this->get_entities_11();
		GameObject_t1113636619 * L_10 = V_1;
		List_1_Add_m2765963565(L_9, L_10, /*hidden argument*/List_1_Add_m2765963565_RuntimeMethod_var);
		GameObject_t1113636619 * L_11 = V_1;
		return L_11;
	}
}
// System.Void Level::fillPillar(Level/TileType[0...,0...],System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Level_fillPillar_m2407563153 (Level_t2237665516 * __this, TileTypeU5B0___U2C0___U5D_t2292257856* ___map0, int32_t ___maxWidth1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector2Int_t3469998543  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_0 = ___map0;
		int32_t L_1 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = ___maxWidth1;
		int32_t L_3 = Random_Range_m4054026115(NULL /*static, unused*/, 1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_0;
		int32_t L_5 = ___maxWidth1;
		int32_t L_6 = Random_Range_m4054026115(NULL /*static, unused*/, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)L_5)), /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		int32_t L_8 = ___maxWidth1;
		int32_t L_9 = Random_Range_m4054026115(NULL /*static, unused*/, 0, ((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)L_8)), /*hidden argument*/NULL);
		Vector2Int__ctor_m3872920888((Vector2Int_t3469998543 *)(&V_2), L_6, L_9, /*hidden argument*/NULL);
		TileTypeU5B0___U2C0___U5D_t2292257856* L_10 = ___map0;
		int32_t L_11 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_12 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_13 = ___maxWidth1;
		int32_t L_14 = ___maxWidth1;
		Level_fill_m1857042071(__this, L_10, L_11, L_12, L_13, L_14, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::Generate(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Level_Generate_m932420581 (Level_t2237665516 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_Generate_m932420581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TileTypeU5B0___U2C0___U5D_t2292257856* V_0 = NULL;
	int32_t V_1 = 0;
	Vector2Int_t3469998543  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2Int_t3469998543  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Vector2Int_t3469998543  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Vector3Int_t741115188  V_10;
	memset(&V_10, 0, sizeof(V_10));
	GameObject_t1113636619 * V_11 = NULL;
	Water_t1083516957 * V_12 = NULL;
	GameObject_t1113636619 * V_13 = NULL;
	int32_t V_14 = 0;
	Vector2_t2156229523  V_15;
	memset(&V_15, 0, sizeof(V_15));
	{
		int32_t L_0 = ___size0;
		int32_t L_1 = ___size0;
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)L_0, (il2cpp_array_size_t)L_1 };
		TileTypeU5B0___U2C0___U5D_t2292257856* L_2 = (TileTypeU5B0___U2C0___U5D_t2292257856*)GenArrayNew(TileTypeU5B0___U2C0___U5D_t2292257856_il2cpp_TypeInfo_var, L_3);
		V_0 = L_2;
		TileTypeU5B0___U2C0___U5D_t2292257856* L_4 = V_0;
		int32_t L_5 = ___size0;
		int32_t L_6 = ___size0;
		Level_fill_m1857042071(__this, L_4, 0, 0, L_5, L_6, 0, /*hidden argument*/NULL);
		TileTypeU5B0___U2C0___U5D_t2292257856* L_7 = V_0;
		int32_t L_8 = ___size0;
		int32_t L_9 = ___size0;
		Level_fill_m1857042071(__this, L_7, ((int32_t)((int32_t)L_8/(int32_t)2)), ((int32_t)((int32_t)L_9/(int32_t)2)), ((int32_t)10), ((int32_t)10), 1, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_003b;
	}

IL_002d:
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_10 = V_0;
		Level_cutChannel_m3357319375(__this, L_10, 5, ((int32_t)15), /*hidden argument*/NULL);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)20))))
		{
			goto IL_002d;
		}
	}
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_13 = V_0;
		Vector2Int_t3469998543  L_14 = Level_find_m3383925018(__this, L_13, 1, /*hidden argument*/NULL);
		V_2 = L_14;
		Vector2Int_t3469998543  L_15 = V_2;
		V_3 = L_15;
		V_4 = 0;
		goto IL_00c8;
	}

IL_0056:
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_16 = V_0;
		Vector2Int_t3469998543  L_17 = Level_find_m3383925018(__this, L_16, 1, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_19 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_20 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)L_19)), /*hidden argument*/NULL);
		int32_t L_21 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_22 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_3), /*hidden argument*/NULL);
		int32_t L_23 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)L_22)), /*hidden argument*/NULL);
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)L_23));
		int32_t L_24 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_25 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_5), /*hidden argument*/NULL);
		int32_t L_26 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_24, (int32_t)L_25)), /*hidden argument*/NULL);
		int32_t L_27 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_28 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_5), /*hidden argument*/NULL);
		int32_t L_29 = Mathf_Abs_m2460432655(NULL /*static, unused*/, ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)L_28)), /*hidden argument*/NULL);
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)L_29));
		int32_t L_30 = V_7;
		int32_t L_31 = V_6;
		if ((((int32_t)L_30) <= ((int32_t)L_31)))
		{
			goto IL_00c2;
		}
	}
	{
		Vector2Int_t3469998543  L_32 = V_5;
		V_3 = L_32;
	}

IL_00c2:
	{
		int32_t L_33 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_33, (int32_t)1));
	}

IL_00c8:
	{
		int32_t L_34 = V_4;
		if ((((int32_t)L_34) < ((int32_t)((int32_t)10))))
		{
			goto IL_0056;
		}
	}
	{
		Tilemap_t1578933799 * L_35 = __this->get_floorMap_7();
		Tilemap_ClearAllTiles_m1382480930(L_35, /*hidden argument*/NULL);
		Tilemap_t1578933799 * L_36 = __this->get_wallMap_8();
		Tilemap_ClearAllTiles_m1382480930(L_36, /*hidden argument*/NULL);
		V_8 = 0;
		goto IL_014c;
	}

IL_00ef:
	{
		V_9 = 0;
		goto IL_013e;
	}

IL_00f7:
	{
		int32_t L_37 = V_8;
		int32_t L_38 = V_9;
		Vector3Int__ctor_m2885707673((Vector3Int_t741115188 *)(&V_10), L_37, L_38, 0, /*hidden argument*/NULL);
		Tilemap_t1578933799 * L_39 = __this->get_floorMap_7();
		Vector3Int_t741115188  L_40 = V_10;
		Tile_t1378929773 * L_41 = __this->get_floor_4();
		Tilemap_SetTile_m1236447073(L_39, L_40, L_41, /*hidden argument*/NULL);
		TileTypeU5B0___U2C0___U5D_t2292257856* L_42 = V_0;
		int32_t L_43 = V_8;
		int32_t L_44 = V_9;
		int32_t L_45 = (L_42)->GetAtUnchecked(L_43, L_44);
		if (L_45)
		{
			goto IL_0138;
		}
	}
	{
		Tilemap_t1578933799 * L_46 = __this->get_wallMap_8();
		Vector3Int_t741115188  L_47 = V_10;
		Tile_t1378929773 * L_48 = __this->get_wall_5();
		Tilemap_SetTile_m1236447073(L_46, L_47, L_48, /*hidden argument*/NULL);
	}

IL_0138:
	{
		int32_t L_49 = V_9;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_013e:
	{
		int32_t L_50 = V_9;
		int32_t L_51 = ___size0;
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_00f7;
		}
	}
	{
		int32_t L_52 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_52, (int32_t)1));
	}

IL_014c:
	{
		int32_t L_53 = V_8;
		int32_t L_54 = ___size0;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_00ef;
		}
	}
	{
		Player_t3266647312 * L_55 = __this->get_player_9();
		Transform_t3600365921 * L_56 = Component_get_transform_m3162698980(L_55, /*hidden argument*/NULL);
		int32_t L_57 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_58 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		Vector3_t3722313464  L_59;
		memset(&L_59, 0, sizeof(L_59));
		Vector3__ctor_m3353183577((&L_59), (((float)((float)L_57))), (((float)((float)L_58))), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_56, L_59, /*hidden argument*/NULL);
		Object_t631007953 * L_60 = Resources_Load_m3880010804(NULL /*static, unused*/, _stringLiteral3191826632, /*hidden argument*/NULL);
		V_11 = ((GameObject_t1113636619 *)IsInstSealed((RuntimeObject*)L_60, GameObject_t1113636619_il2cpp_TypeInfo_var));
		GameObject_t1113636619 * L_61 = V_11;
		int32_t L_62 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		int32_t L_63 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_2), /*hidden argument*/NULL);
		Vector3_t3722313464  L_64;
		memset(&L_64, 0, sizeof(L_64));
		Vector3__ctor_m1719387948((&L_64), (((float)((float)L_62))), (((float)((float)L_63))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_65 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_64, /*hidden argument*/NULL);
		Level_instantiate_m2506382120(__this, L_61, L_65, _stringLiteral4086148775, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_66 = V_11;
		int32_t L_67 = Vector2Int_get_x_m64542184((Vector2Int_t3469998543 *)(&V_3), /*hidden argument*/NULL);
		int32_t L_68 = Vector2Int_get_y_m64542185((Vector2Int_t3469998543 *)(&V_3), /*hidden argument*/NULL);
		Vector2_t2156229523  L_69;
		memset(&L_69, 0, sizeof(L_69));
		Vector2__ctor_m3970636864((&L_69), (((float)((float)L_67))), (((float)((float)L_68))), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_70 = Level_instantiate_m2506382120(__this, L_66, L_69, _stringLiteral1031853882, /*hidden argument*/NULL);
		Water_t1083516957 * L_71 = GameObject_GetComponent_TisWater_t1083516957_m400380985(L_70, /*hidden argument*/GameObject_GetComponent_TisWater_t1083516957_m400380985_RuntimeMethod_var);
		V_12 = L_71;
		Water_t1083516957 * L_72 = V_12;
		intptr_t L_73 = (intptr_t)Level_U3CGenerateU3Em__1_m2845411788_RuntimeMethod_var;
		Activator_t2573376075 * L_74 = (Activator_t2573376075 *)il2cpp_codegen_object_new(Activator_t2573376075_il2cpp_TypeInfo_var);
		Activator__ctor_m1355085645(L_74, __this, L_73, /*hidden argument*/NULL);
		Water_SetActivator_m3781101325(L_72, L_74, /*hidden argument*/NULL);
		Object_t631007953 * L_75 = Resources_Load_m3880010804(NULL /*static, unused*/, _stringLiteral3701375849, /*hidden argument*/NULL);
		V_13 = ((GameObject_t1113636619 *)IsInstSealed((RuntimeObject*)L_75, GameObject_t1113636619_il2cpp_TypeInfo_var));
		V_14 = 0;
		goto IL_023d;
	}

IL_020c:
	{
		TileTypeU5B0___U2C0___U5D_t2292257856* L_76 = V_0;
		Vector2Int_t3469998543  L_77 = Level_find_m3383925018(__this, L_76, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_78 = Vector2Int_op_Implicit_m1284226264(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		V_15 = L_78;
		GameObject_t1113636619 * L_79 = V_13;
		Vector2_t2156229523  L_80 = V_15;
		int32_t L_81 = V_14;
		int32_t L_82 = L_81;
		RuntimeObject * L_83 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_82);
		String_t* L_84 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral27031491, L_83, /*hidden argument*/NULL);
		Level_instantiate_m2506382120(__this, L_79, L_80, L_84, /*hidden argument*/NULL);
		int32_t L_85 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add((int32_t)L_85, (int32_t)1));
	}

IL_023d:
	{
		int32_t L_86 = V_14;
		if ((((int32_t)L_86) < ((int32_t)5)))
		{
			goto IL_020c;
		}
	}
	{
		return;
	}
}
// System.Void Level::<Deactivate>m__0(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR void Level_U3CDeactivateU3Em__0_m4082416521 (RuntimeObject * __this /* static, unused */, GameObject_t1113636619 * ___gameObject0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Level_U3CDeactivateU3Em__0_m4082416521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = ___gameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m565254235(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Level::<Generate>m__1()
extern "C" IL2CPP_METHOD_ATTR void Level_U3CGenerateU3Em__1_m2845411788 (Level_t2237665516 * __this, const RuntimeMethod* method)
{
	{
		Level_Init_m3675134387(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MainMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainMenu__ctor_m2617117415 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single MainMenu::get_menuDuration()
extern "C" IL2CPP_METHOD_ATTR float MainMenu_get_menuDuration_m1640785792 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CmenuDurationU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void MainMenu::set_menuDuration(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainMenu_set_menuDuration_m1906417042 (MainMenu_t3798339593 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CmenuDurationU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Single MainMenu::get_blinkTime()
extern "C" IL2CPP_METHOD_ATTR float MainMenu_get_blinkTime_m416367648 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CblinkTimeU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void MainMenu::set_blinkTime(System.Single)
extern "C" IL2CPP_METHOD_ATTR void MainMenu_set_blinkTime_m377316010 (MainMenu_t3798339593 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CblinkTimeU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void MainMenu::Start()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Start_m2591133275 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		MainMenu_Init_m4196376439(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::Init()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Init_m4196376439 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	{
		MainMenu_set_menuDuration_m1906417042(__this, (2.0f), /*hidden argument*/NULL);
		MainMenu_set_blinkTime_m377316010(__this, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void MainMenu::Update()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_Update_m1422991247 (MainMenu_t3798339593 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_Update_m1422991247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m2064261504(NULL /*static, unused*/, _stringLiteral39716438, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		float L_2 = MainMenu_get_blinkTime_m416367648(__this, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		MainMenu_set_blinkTime_m377316010(__this, ((float)il2cpp_codegen_add((float)L_2, (float)L_3)), /*hidden argument*/NULL);
		goto IL_0033;
	}

IL_0028:
	{
		MainMenu_set_blinkTime_m377316010(__this, (0.0f), /*hidden argument*/NULL);
	}

IL_0033:
	{
		float L_4 = MainMenu_get_blinkTime_m416367648(__this, /*hidden argument*/NULL);
		float L_5 = MainMenu_get_menuDuration_m1640785792(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_006c;
		}
	}
	{
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		GameObject_SetActive_m796801857(L_6, (bool)0, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_7 = __this->get_game_5();
		GameObject_SetActive_m796801857(L_7, (bool)1, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_8 = __this->get_game_5();
		GameScreen_t1800529819 * L_9 = GameObject_GetComponent_TisGameScreen_t1800529819_m2973029269(L_8, /*hidden argument*/GameObject_GetComponent_TisGameScreen_t1800529819_m2973029269_RuntimeMethod_var);
		GameScreen_Init_m2409608521(L_9, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MenuCamera::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MenuCamera__ctor_m3326497400 (MenuCamera_t4282292518 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuCamera::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void MenuCamera_OnRenderImage_m2799149731 (MenuCamera_t4282292518 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuCamera_OnRenderImage_m2799149731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MainMenu_t3798339593 * V_0 = NULL;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		GameObject_t1113636619 * L_0 = __this->get_menu_4();
		MainMenu_t3798339593 * L_1 = GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124(L_0, /*hidden argument*/GameObject_GetComponent_TisMainMenu_t3798339593_m1294621124_RuntimeMethod_var);
		V_0 = L_1;
		MainMenu_t3798339593 * L_2 = V_0;
		float L_3 = MainMenu_get_menuDuration_m1640785792(L_2, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_3/(float)(2.0f)));
		MainMenu_t3798339593 * L_4 = V_0;
		float L_5 = MainMenu_get_blinkTime_m416367648(L_4, /*hidden argument*/NULL);
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Max_m3146388979(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)), (0.0f), /*hidden argument*/NULL);
		MainMenu_t3798339593 * L_8 = V_0;
		float L_9 = MainMenu_get_menuDuration_m1640785792(L_8, /*hidden argument*/NULL);
		float L_10 = V_1;
		V_2 = ((float)((float)L_7/(float)((float)il2cpp_codegen_subtract((float)L_9, (float)L_10))));
		Material_t340375123 * L_11 = __this->get_menuMaterial_5();
		float L_12 = V_2;
		Material_SetFloat_m3226510453(L_11, _stringLiteral3522276777, L_12, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_13 = ___source0;
		RenderTexture_t2108887433 * L_14 = ___destination1;
		Material_t340375123 * L_15 = __this->get_menuMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m890955694(NULL /*static, unused*/, L_13, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Monster::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Monster__ctor_m2987639624 (Monster_t1049719775 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster__ctor_m2987639624_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_maxStateQueue_4((3.0f));
		__this->set_minStateQueue_5((1.0f));
		__this->set_fov_6((40.0f));
		List_1_t2585711361 * L_0 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m1424466557(L_0, /*hidden argument*/List_1__ctor_m1424466557_RuntimeMethod_var);
		__this->set_aware_9(L_0);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Monster::Start()
extern "C" IL2CPP_METHOD_ATTR void Monster_Start_m2410673422 (Monster_t1049719775 * __this, const RuntimeMethod* method)
{
	{
		Monster_Init_m664643989(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Monster::Init()
extern "C" IL2CPP_METHOD_ATTR void Monster_Init_m664643989 (Monster_t1049719775 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster_Init_m664643989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Rigidbody2D_t939494601 * L_0 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rigidcollider_7(L_0);
		CircleCollider2D_t662546754 * L_1 = Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155(__this, /*hidden argument*/Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155_RuntimeMethod_var);
		__this->set_vision_8(L_1);
		AudioSource_t3935305588 * L_2 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		__this->set_howl_14(L_2);
		CircleCollider2D_t662546754 * L_3 = __this->get_vision_8();
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(L_3, /*hidden argument*/NULL);
		intptr_t L_5 = (intptr_t)Monster_U3CInitU3Em__0_m4099899884_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_6 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_6, __this, L_5, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerEnter2D_m1969312576(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		CircleCollider2D_t662546754 * L_7 = __this->get_vision_8();
		GameObject_t1113636619 * L_8 = Component_get_gameObject_m442555142(L_7, /*hidden argument*/NULL);
		intptr_t L_9 = (intptr_t)Monster_U3CInitU3Em__1_m3061626409_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_10 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_10, __this, L_9, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerExit2D_m3161418375(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Monster::facing(UnityEngine.GameObject)
extern "C" IL2CPP_METHOD_ATTR bool Monster_facing_m1121058899 (Monster_t1049719775 * __this, GameObject_t1113636619 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster_facing_m1121058899_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	Quaternion_t2301928331  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3722313464  V_4;
	memset(&V_4, 0, sizeof(V_4));
	RaycastHit2DU5BU5D_t4286651560* V_5 = NULL;
	RaycastHit2D_t2279581989  V_6;
	memset(&V_6, 0, sizeof(V_6));
	RaycastHit2DU5BU5D_t4286651560* V_7 = NULL;
	int32_t V_8 = 0;
	RaycastHit2DU5BU5D_t4286651560* G_B6_0 = NULL;
	RaycastHit2DU5BU5D_t4286651560* G_B5_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = ___obj0;
		Transform_t3600365921 * L_1 = GameObject_get_transform_m1369836730(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_5 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = (&V_0)->get_y_3();
		float L_7 = (&V_0)->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_8 = atan2f(L_6, L_7);
		V_1 = ((float)((float)((float)il2cpp_codegen_multiply((float)L_8, (float)(180.0f)))/(float)(3.14159274f)));
		Transform_t3600365921 * L_9 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_10 = Transform_get_rotation_m3502953881(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		Vector3_t3722313464  L_11 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&V_3), /*hidden argument*/NULL);
		V_4 = L_11;
		float L_12 = (&V_4)->get_z_4();
		V_2 = L_12;
		float L_13 = V_1;
		V_1 = (fmodf(L_13, (360.0f)));
		float L_14 = V_1;
		if ((!(((float)L_14) < ((float)(0.0f)))))
		{
			goto IL_0074;
		}
	}
	{
		float L_15 = V_1;
		V_1 = ((float)il2cpp_codegen_add((float)L_15, (float)(360.0f)));
	}

IL_0074:
	{
		float L_16 = V_1;
		float L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = fabsf(((float)il2cpp_codegen_subtract((float)L_16, (float)L_17)));
		float L_19 = __this->get_fov_6();
		if ((((float)((float)il2cpp_codegen_subtract((float)(360.0f), (float)L_18))) < ((float)L_19)))
		{
			goto IL_00a0;
		}
	}
	{
		float L_20 = V_1;
		float L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = fabsf(((float)il2cpp_codegen_subtract((float)L_20, (float)L_21)));
		float L_23 = __this->get_fov_6();
		if ((!(((float)L_22) < ((float)L_23))))
		{
			goto IL_0169;
		}
	}

IL_00a0:
	{
		Transform_t3600365921 * L_24 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_25 = Transform_get_position_m36019626(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_26 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Vector3_t3722313464  L_27 = V_0;
		Vector2_t2156229523  L_28 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_29 = Physics2D_RaycastAll_m3534418674(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		V_5 = L_29;
		RaycastHit2DU5BU5D_t4286651560* L_30 = V_5;
		Comparison_1_t2054513168 * L_31 = ((Monster_t1049719775_StaticFields*)il2cpp_codegen_static_fields_for(Monster_t1049719775_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_15();
		G_B5_0 = L_30;
		if (L_31)
		{
			G_B6_0 = L_30;
			goto IL_00d7;
		}
	}
	{
		intptr_t L_32 = (intptr_t)Monster_U3CfacingU3Em__2_m166827732_RuntimeMethod_var;
		Comparison_1_t2054513168 * L_33 = (Comparison_1_t2054513168 *)il2cpp_codegen_object_new(Comparison_1_t2054513168_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1180004629(L_33, NULL, L_32, /*hidden argument*/Comparison_1__ctor_m1180004629_RuntimeMethod_var);
		((Monster_t1049719775_StaticFields*)il2cpp_codegen_static_fields_for(Monster_t1049719775_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_15(L_33);
		G_B6_0 = G_B5_0;
	}

IL_00d7:
	{
		Comparison_1_t2054513168 * L_34 = ((Monster_t1049719775_StaticFields*)il2cpp_codegen_static_fields_for(Monster_t1049719775_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_15();
		Array_Sort_TisRaycastHit2D_t2279581989_m3365424645(NULL /*static, unused*/, G_B6_0, L_34, /*hidden argument*/Array_Sort_TisRaycastHit2D_t2279581989_m3365424645_RuntimeMethod_var);
		RaycastHit2DU5BU5D_t4286651560* L_35 = V_5;
		V_7 = L_35;
		V_8 = 0;
		goto IL_015e;
	}

IL_00ed:
	{
		RaycastHit2DU5BU5D_t4286651560* L_36 = V_7;
		int32_t L_37 = V_8;
		V_6 = (*(RaycastHit2D_t2279581989 *)((L_36)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_37))));
		Transform_t3600365921 * L_38 = RaycastHit2D_get_transform_m2048267409((RaycastHit2D_t2279581989 *)(&V_6), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_39 = Component_get_gameObject_m442555142(L_38, /*hidden argument*/NULL);
		String_t* L_40 = GameObject_get_tag_m3951609671(L_39, /*hidden argument*/NULL);
		bool L_41 = String_op_Equality_m920492651(NULL /*static, unused*/, L_40, _stringLiteral1201842151, /*hidden argument*/NULL);
		if (L_41)
		{
			goto IL_013d;
		}
	}
	{
		Transform_t3600365921 * L_42 = RaycastHit2D_get_transform_m2048267409((RaycastHit2D_t2279581989 *)(&V_6), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_43 = Component_get_gameObject_m442555142(L_42, /*hidden argument*/NULL);
		String_t* L_44 = GameObject_get_tag_m3951609671(L_43, /*hidden argument*/NULL);
		bool L_45 = String_op_Equality_m920492651(NULL /*static, unused*/, L_44, _stringLiteral78692563, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_013f;
		}
	}

IL_013d:
	{
		return (bool)0;
	}

IL_013f:
	{
		Transform_t3600365921 * L_46 = RaycastHit2D_get_transform_m2048267409((RaycastHit2D_t2279581989 *)(&V_6), /*hidden argument*/NULL);
		GameObject_t1113636619 * L_47 = Component_get_gameObject_m442555142(L_46, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_48 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_47, L_48, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0158;
		}
	}
	{
		return (bool)1;
	}

IL_0158:
	{
		int32_t L_50 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1));
	}

IL_015e:
	{
		int32_t L_51 = V_8;
		RaycastHit2DU5BU5D_t4286651560* L_52 = V_7;
		if ((((int32_t)L_51) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_52)->max_length)))))))
		{
			goto IL_00ed;
		}
	}

IL_0169:
	{
		return (bool)0;
	}
}
// System.Void Monster::Update()
extern "C" IL2CPP_METHOD_ATTR void Monster_Update_m1264681689 (Monster_t1049719775 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster_Update_m1264681689_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * V_1 = NULL;
	Enumerator_t179987942  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Quaternion_t2301928331  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2156229523  V_6;
	memset(&V_6, 0, sizeof(V_6));
	int32_t V_7 = 0;
	Vector2_t2156229523  V_8;
	memset(&V_8, 0, sizeof(V_8));
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Quaternion_t2301928331  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Quaternion_t2301928331  V_13;
	memset(&V_13, 0, sizeof(V_13));
	RuntimeArray * V_14 = NULL;
	int32_t V_15 = 0;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B19_0 = 0;
	int32_t G_B18_0 = 0;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	Monster_t1049719775 * G_B24_0 = NULL;
	Monster_t1049719775 * G_B23_0 = NULL;
	int32_t G_B25_0 = 0;
	Monster_t1049719775 * G_B25_1 = NULL;
	{
		Rigidbody2D_t939494601 * L_0 = __this->get_rigidcollider_7();
		Vector2_t2156229523  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m3970636864((&L_1), (0.0f), (0.0f), /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m2898400508(L_0, L_1, /*hidden argument*/NULL);
		Rigidbody2D_t939494601 * L_2 = __this->get_rigidcollider_7();
		Rigidbody2D_set_angularVelocity_m2791812150(L_2, (0.0f), /*hidden argument*/NULL);
		V_0 = (GameObject_t1113636619 *)NULL;
		List_1_t2585711361 * L_3 = __this->get_aware_9();
		Enumerator_t179987942  L_4 = List_1_GetEnumerator_m1750140655(L_3, /*hidden argument*/List_1_GetEnumerator_m1750140655_RuntimeMethod_var);
		V_2 = L_4;
	}

IL_0038:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0068;
		}

IL_003d:
		{
			GameObject_t1113636619 * L_5 = Enumerator_get_Current_m4179928398((Enumerator_t179987942 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m4179928398_RuntimeMethod_var);
			V_1 = L_5;
			GameObject_t1113636619 * L_6 = V_1;
			String_t* L_7 = GameObject_get_tag_m3951609671(L_6, /*hidden argument*/NULL);
			bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, L_7, _stringLiteral2261822918, /*hidden argument*/NULL);
			if (!L_8)
			{
				goto IL_0068;
			}
		}

IL_005a:
		{
			GameObject_t1113636619 * L_9 = V_1;
			bool L_10 = Monster_facing_m1121058899(__this, L_9, /*hidden argument*/NULL);
			if (!L_10)
			{
				goto IL_0068;
			}
		}

IL_0066:
		{
			GameObject_t1113636619 * L_11 = V_1;
			V_0 = L_11;
		}

IL_0068:
		{
			bool L_12 = Enumerator_MoveNext_m4286844348((Enumerator_t179987942 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m4286844348_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_003d;
			}
		}

IL_0074:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1341201278((Enumerator_t179987942 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m1341201278_RuntimeMethod_var);
		IL2CPP_END_FINALLY(121)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0087:
	{
		GameObject_t1113636619 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_13, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0111;
		}
	}
	{
		GameObject_t1113636619 * L_15 = __this->get_chargeTarget_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_15, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0111;
		}
	}
	{
		GameObject_t1113636619 * L_17 = V_0;
		Transform_t3600365921 * L_18 = GameObject_get_transform_m1369836730(L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Transform_get_position_m36019626(L_18, /*hidden argument*/NULL);
		Transform_t3600365921 * L_20 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_21 = Transform_get_position_m36019626(L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t3722313464_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_22 = Vector3_op_Subtraction_m3073674971(NULL /*static, unused*/, L_19, L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_y_3();
		float L_24 = (&V_3)->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_25 = atan2f(L_23, L_24);
		V_4 = ((float)((float)((float)il2cpp_codegen_multiply((float)L_25, (float)(180.0f)))/(float)(3.14159274f)));
		float L_26 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_27 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), (0.0f), L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		Rigidbody2D_t939494601 * L_28 = __this->get_rigidcollider_7();
		Transform_t3600365921 * L_29 = Component_get_transform_m3162698980(L_28, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_30 = V_5;
		Transform_set_rotation_m3524318132(L_29, L_30, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_31 = __this->get_howl_14();
		AudioSource_Play_m48294159(L_31, /*hidden argument*/NULL);
	}

IL_0111:
	{
		GameObject_t1113636619 * L_32 = V_0;
		__this->set_chargeTarget_13(L_32);
		GameObject_t1113636619 * L_33 = __this->get_chargeTarget_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_34 = Object_op_Implicit_m3574996620(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0165;
		}
	}
	{
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_6), (15.0f), (0.0f), /*hidden argument*/NULL);
		Transform_t3600365921 * L_35 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_36 = Transform_get_rotation_m3502953881(L_35, /*hidden argument*/NULL);
		Vector2_t2156229523  L_37 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_38 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_39 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_36, L_38, /*hidden argument*/NULL);
		Vector2_t2156229523  L_40 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
		V_6 = L_40;
		Rigidbody2D_t939494601 * L_41 = __this->get_rigidcollider_7();
		Vector2_t2156229523  L_42 = V_6;
		Rigidbody2D_set_velocity_m2898400508(L_41, L_42, /*hidden argument*/NULL);
		return;
	}

IL_0165:
	{
		int32_t L_43 = __this->get_state_10();
		V_7 = L_43;
		int32_t L_44 = V_7;
		if ((((int32_t)L_44) == ((int32_t)2)))
		{
			goto IL_0182;
		}
	}
	{
		int32_t L_45 = V_7;
		if ((((int32_t)L_45) == ((int32_t)1)))
		{
			goto IL_01c3;
		}
	}
	{
		goto IL_0232;
	}

IL_0182:
	{
		Vector2__ctor_m3970636864((Vector2_t2156229523 *)(&V_8), (3.0f), (0.0f), /*hidden argument*/NULL);
		Transform_t3600365921 * L_46 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_47 = Transform_get_rotation_m3502953881(L_46, /*hidden argument*/NULL);
		Vector2_t2156229523  L_48 = V_8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_49 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Vector3_t3722313464  L_50 = Quaternion_op_Multiply_m2607404835(NULL /*static, unused*/, L_47, L_49, /*hidden argument*/NULL);
		Vector2_t2156229523  L_51 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		V_8 = L_51;
		Rigidbody2D_t939494601 * L_52 = __this->get_rigidcollider_7();
		Vector2_t2156229523  L_53 = V_8;
		Rigidbody2D_set_velocity_m2898400508(L_52, L_53, /*hidden argument*/NULL);
		goto IL_0232;
	}

IL_01c3:
	{
		bool L_54 = __this->get_rotationDirection_12();
		G_B18_0 = ((int32_t)36);
		if (!L_54)
		{
			G_B19_0 = ((int32_t)36);
			goto IL_01d6;
		}
	}
	{
		G_B20_0 = 1;
		G_B20_1 = G_B18_0;
		goto IL_01d7;
	}

IL_01d6:
	{
		G_B20_0 = (-1);
		G_B20_1 = G_B19_0;
	}

IL_01d7:
	{
		V_9 = (((float)((float)((int32_t)il2cpp_codegen_multiply((int32_t)G_B20_1, (int32_t)G_B20_0)))));
		Rigidbody2D_t939494601 * L_55 = __this->get_rigidcollider_7();
		Transform_t3600365921 * L_56 = Component_get_transform_m3162698980(L_55, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_57 = Transform_get_rotation_m3502953881(L_56, /*hidden argument*/NULL);
		V_11 = L_57;
		Vector3_t3722313464  L_58 = Quaternion_get_eulerAngles_m3425202016((Quaternion_t2301928331 *)(&V_11), /*hidden argument*/NULL);
		V_12 = L_58;
		float L_59 = (&V_12)->get_z_4();
		float L_60 = V_9;
		float L_61 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_10 = ((float)il2cpp_codegen_add((float)L_59, (float)((float)il2cpp_codegen_multiply((float)L_60, (float)L_61))));
		float L_62 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t2301928331_il2cpp_TypeInfo_var);
		Quaternion_t2301928331  L_63 = Quaternion_Euler_m3049309462(NULL /*static, unused*/, (0.0f), (0.0f), L_62, /*hidden argument*/NULL);
		V_13 = L_63;
		Rigidbody2D_t939494601 * L_64 = __this->get_rigidcollider_7();
		Transform_t3600365921 * L_65 = Component_get_transform_m3162698980(L_64, /*hidden argument*/NULL);
		Quaternion_t2301928331  L_66 = V_13;
		Transform_set_rotation_m3524318132(L_65, L_66, /*hidden argument*/NULL);
		goto IL_0232;
	}

IL_0232:
	{
		float L_67 = __this->get_stateRemaining_11();
		float L_68 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_stateRemaining_11(((float)il2cpp_codegen_subtract((float)L_67, (float)L_68)));
		float L_69 = __this->get_stateRemaining_11();
		if ((!(((float)L_69) <= ((float)(0.0f)))))
		{
			goto IL_02c3;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_70 = { reinterpret_cast<intptr_t> (MonsterState_t2135005343_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_71 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeArray * L_72 = Enum_GetValues_m4192343468(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		V_14 = L_72;
		RuntimeArray * L_73 = V_14;
		int32_t L_74 = Array_get_Length_m21610649(L_73, /*hidden argument*/NULL);
		int32_t L_75 = Random_Range_m4054026115(NULL /*static, unused*/, 0, L_74, /*hidden argument*/NULL);
		V_15 = L_75;
		RuntimeArray * L_76 = V_14;
		int32_t L_77 = V_15;
		RuntimeObject * L_78 = Array_GetValue_m2528546681(L_76, L_77, /*hidden argument*/NULL);
		__this->set_state_10(((*(int32_t*)((int32_t*)UnBox(L_78, MonsterState_t2135005343_il2cpp_TypeInfo_var)))));
		float L_79 = __this->get_minStateQueue_5();
		float L_80 = __this->get_maxStateQueue_4();
		float L_81 = Random_Range_m2202990745(NULL /*static, unused*/, L_79, L_80, /*hidden argument*/NULL);
		__this->set_stateRemaining_11(L_81);
		int32_t L_82 = Random_Range_m4054026115(NULL /*static, unused*/, 0, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_83 = bankers_roundf((((float)((float)L_82))));
		G_B23_0 = __this;
		if ((!(((float)L_83) == ((float)(1.0f)))))
		{
			G_B24_0 = __this;
			goto IL_02bd;
		}
	}
	{
		G_B25_0 = 1;
		G_B25_1 = G_B23_0;
		goto IL_02be;
	}

IL_02bd:
	{
		G_B25_0 = 0;
		G_B25_1 = G_B24_0;
	}

IL_02be:
	{
		G_B25_1->set_rotationDirection_12((bool)G_B25_0);
	}

IL_02c3:
	{
		return;
	}
}
// System.Void Monster::<Init>m__0(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Monster_U3CInitU3Em__0_m4099899884 (Monster_t1049719775 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster_U3CInitU3Em__0_m4099899884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t2585711361 * L_2 = __this->get_aware_9();
		GameObject_t1113636619 * L_3 = V_0;
		List_1_Add_m2765963565(L_2, L_3, /*hidden argument*/List_1_Add_m2765963565_RuntimeMethod_var);
		return;
	}
}
// System.Void Monster::<Init>m__1(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Monster_U3CInitU3Em__1_m3061626409 (Monster_t1049719775 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Monster_U3CInitU3Em__1_m3061626409_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		List_1_t2585711361 * L_2 = __this->get_aware_9();
		GameObject_t1113636619 * L_3 = V_0;
		List_1_Remove_m4063777476(L_2, L_3, /*hidden argument*/List_1_Remove_m4063777476_RuntimeMethod_var);
		return;
	}
}
// System.Int32 Monster::<facing>m__2(UnityEngine.RaycastHit2D,UnityEngine.RaycastHit2D)
extern "C" IL2CPP_METHOD_ATTR int32_t Monster_U3CfacingU3Em__2_m166827732 (RuntimeObject * __this /* static, unused */, RaycastHit2D_t2279581989  ___ob10, RaycastHit2D_t2279581989  ___ob21, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = RaycastHit2D_get_distance_m382898860((RaycastHit2D_t2279581989 *)(&___ob10), /*hidden argument*/NULL);
		float L_1 = RaycastHit2D_get_distance_m382898860((RaycastHit2D_t2279581989 *)(&___ob21), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0019;
		}
	}
	{
		G_B3_0 = 1;
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Player::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Player__ctor_m509448900 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player__ctor_m509448900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2585711361 * L_0 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m1424466557(L_0, /*hidden argument*/List_1__ctor_m1424466557_RuntimeMethod_var);
		__this->set_monsters_13(L_0);
		__this->set_minHeartrate_16((1.2f));
		__this->set_maxHeartrate_17((0.428571433f));
		__this->set_speed_18((3.5f));
		__this->set_recoverDelta_19((0.01f));
		__this->set_blinkPenalty_20((0.02f));
		__this->set_restingStressVelocity_21((0.01f));
		__this->set_waterBonus_22((0.2f));
		__this->set_waterStressVelocity_23((-0.01f));
		__this->set_gracePeriod_24((1.5f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Player::get_stress()
extern "C" IL2CPP_METHOD_ATTR float Player_get_stress_m4229801089 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CstressU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void Player::set_stress(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Player_set_stress_m4170066995 (Player_t3266647312 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CstressU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single Player::get_blink()
extern "C" IL2CPP_METHOD_ATTR float Player_get_blink_m816255482 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = __this->get_U3CblinkU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Player::set_blink(System.Single)
extern "C" IL2CPP_METHOD_ATTR void Player_set_blink_m3504210868 (Player_t3266647312 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		float L_0 = ___value0;
		__this->set_U3CblinkU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Int32 Player::get_poolsDiscovered()
extern "C" IL2CPP_METHOD_ATTR int32_t Player_get_poolsDiscovered_m1228037983 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CpoolsDiscoveredU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void Player::set_poolsDiscovered(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Player_set_poolsDiscovered_m3631500643 (Player_t3266647312 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CpoolsDiscoveredU3Ek__BackingField_11(L_0);
		return;
	}
}
// System.Void Player::Start()
extern "C" IL2CPP_METHOD_ATTR void Player_Start_m250748966 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	{
		Player_Init_m3348666991(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::Init()
extern "C" IL2CPP_METHOD_ATTR void Player_Init_m3348666991 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_Init_m3348666991_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Player_set_stress_m4170066995(__this, (0.0f), /*hidden argument*/NULL);
		Player_set_blink_m3504210868(__this, (1.0f), /*hidden argument*/NULL);
		Player_set_poolsDiscovered_m3631500643(__this, 0, /*hidden argument*/NULL);
		__this->set_pool_12((Water_t1083516957 *)NULL);
		List_1_t2585711361 * L_0 = (List_1_t2585711361 *)il2cpp_codegen_object_new(List_1_t2585711361_il2cpp_TypeInfo_var);
		List_1__ctor_m1424466557(L_0, /*hidden argument*/List_1__ctor_m1424466557_RuntimeMethod_var);
		__this->set_monsters_13(L_0);
		Rigidbody2D_t939494601 * L_1 = Component_GetComponent_TisRigidbody2D_t939494601_m1531613439(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t939494601_m1531613439_RuntimeMethod_var);
		__this->set_rigidcollider_4(L_1);
		CircleCollider2D_t662546754 * L_2 = Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155(__this, /*hidden argument*/Component_GetComponentInChildren_TisCircleCollider2D_t662546754_m2958246155_RuntimeMethod_var);
		__this->set_vision_5(L_2);
		AudioSource_t3935305588 * L_3 = Component_GetComponent_TisAudioSource_t3935305588_m1977431131(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t3935305588_m1977431131_RuntimeMethod_var);
		__this->set_heartbeat_14(L_3);
		CircleCollider2D_t662546754 * L_4 = __this->get_vision_5();
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m442555142(L_4, /*hidden argument*/NULL);
		CollisionEventer_ClearTriggerEnter2D_m671181426(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		CircleCollider2D_t662546754 * L_6 = __this->get_vision_5();
		GameObject_t1113636619 * L_7 = Component_get_gameObject_m442555142(L_6, /*hidden argument*/NULL);
		intptr_t L_8 = (intptr_t)Player_U3CInitU3Em__0_m3753943790_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_9 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_9, __this, L_8, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerEnter2D_m1969312576(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		CircleCollider2D_t662546754 * L_10 = __this->get_vision_5();
		GameObject_t1113636619 * L_11 = Component_get_gameObject_m442555142(L_10, /*hidden argument*/NULL);
		CollisionEventer_ClearTriggerExit2D_m42893453(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		CircleCollider2D_t662546754 * L_12 = __this->get_vision_5();
		GameObject_t1113636619 * L_13 = Component_get_gameObject_m442555142(L_12, /*hidden argument*/NULL);
		intptr_t L_14 = (intptr_t)Player_U3CInitU3Em__1_m102196894_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_15 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_15, __this, L_14, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerExit2D_m3161418375(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_16 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		CollisionEventer_ClearTriggerEnter2D_m671181426(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_17 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		intptr_t L_18 = (intptr_t)Player_U3CInitU3Em__2_m1137546408_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_19 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_19, __this, L_18, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerEnter2D_m1969312576(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_20 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		CollisionEventer_ClearTriggerExit2D_m42893453(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_21 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		intptr_t L_22 = (intptr_t)Player_U3CInitU3Em__3_m2803041481_RuntimeMethod_var;
		TriggerListener_t1115224763 * L_23 = (TriggerListener_t1115224763 *)il2cpp_codegen_object_new(TriggerListener_t1115224763_il2cpp_TypeInfo_var);
		TriggerListener__ctor_m813156148(L_23, __this, L_22, /*hidden argument*/NULL);
		CollisionEventer_OnTriggerExit2D_m3161418375(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::Update()
extern "C" IL2CPP_METHOD_ATTR void Player_Update_m2111558832 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	{
		Player_PlayerInput_m1067569136(__this, /*hidden argument*/NULL);
		Player_PlayerLogic_m3769131123(__this, /*hidden argument*/NULL);
		Player_PlayerEffects_m1429873540(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::PlayerInput()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerInput_m1067569136 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_PlayerInput_m1067569136_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	bool V_3 = false;
	float G_B4_0 = 0.0f;
	float G_B8_0 = 0.0f;
	Player_t3266647312 * G_B16_0 = NULL;
	Player_t3266647312 * G_B15_0 = NULL;
	int32_t G_B17_0 = 0;
	Player_t3266647312 * G_B17_1 = NULL;
	{
		float L_0 = __this->get_speed_18();
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		float L_1 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral1828639942, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = Input_GetAxisRaw_m2316819917(NULL /*static, unused*/, _stringLiteral2984908384, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = V_1;
		if ((((float)L_3) > ((float)(0.05f))))
		{
			goto IL_0033;
		}
	}
	{
		float L_4 = V_1;
		if ((!(((float)L_4) < ((float)(-0.05f)))))
		{
			goto IL_0039;
		}
	}

IL_0033:
	{
		float L_5 = V_1;
		G_B4_0 = L_5;
		goto IL_003e;
	}

IL_0039:
	{
		G_B4_0 = (0.0f);
	}

IL_003e:
	{
		V_1 = G_B4_0;
		float L_6 = V_2;
		if ((((float)L_6) > ((float)(0.05f))))
		{
			goto IL_0055;
		}
	}
	{
		float L_7 = V_2;
		if ((!(((float)L_7) < ((float)(-0.05f)))))
		{
			goto IL_005b;
		}
	}

IL_0055:
	{
		float L_8 = V_2;
		G_B8_0 = L_8;
		goto IL_0060;
	}

IL_005b:
	{
		G_B8_0 = (0.0f);
	}

IL_0060:
	{
		V_2 = G_B8_0;
		Rigidbody2D_t939494601 * L_9 = __this->get_rigidcollider_4();
		float L_10 = V_1;
		float L_11 = V_0;
		float L_12 = V_2;
		float L_13 = V_0;
		Vector2_t2156229523  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector2__ctor_m3970636864((&L_14), ((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)), ((float)il2cpp_codegen_multiply((float)L_12, (float)L_13)), /*hidden argument*/NULL);
		Rigidbody2D_set_velocity_m2898400508(L_9, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetButton_m2064261504(NULL /*static, unused*/, _stringLiteral39716438, /*hidden argument*/NULL);
		V_3 = L_15;
		bool L_16 = V_3;
		if (!L_16)
		{
			goto IL_00a8;
		}
	}
	{
		float L_17 = Player_get_blink_m816255482(__this, /*hidden argument*/NULL);
		if ((!(((float)L_17) == ((float)(0.0f)))))
		{
			goto IL_00a8;
		}
	}
	{
		__this->set_blinkTime_9((0.0f));
		goto IL_00c9;
	}

IL_00a8:
	{
		bool L_18 = V_3;
		if (L_18)
		{
			goto IL_00c9;
		}
	}
	{
		float L_19 = Player_get_blink_m816255482(__this, /*hidden argument*/NULL);
		if ((!(((float)L_19) == ((float)(1.0f)))))
		{
			goto IL_00c9;
		}
	}
	{
		__this->set_blinkTime_9((0.0f));
	}

IL_00c9:
	{
		bool L_20 = V_3;
		G_B15_0 = __this;
		if (!L_20)
		{
			G_B16_0 = __this;
			goto IL_00d6;
		}
	}
	{
		G_B17_0 = 1;
		G_B17_1 = G_B15_0;
		goto IL_00d7;
	}

IL_00d6:
	{
		G_B17_0 = 0;
		G_B17_1 = G_B16_0;
	}

IL_00d7:
	{
		Player_set_blink_m3504210868(G_B17_1, (((float)((float)G_B17_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::PlayerLogic()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerLogic_m3769131123 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_PlayerLogic_m3769131123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	GameObject_t1113636619 * V_1 = NULL;
	GameObject_t1113636619 * V_2 = NULL;
	Enumerator_t179987942  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	Player_t3266647312 * G_B3_0 = NULL;
	Player_t3266647312 * G_B2_0 = NULL;
	float G_B4_0 = 0.0f;
	Player_t3266647312 * G_B4_1 = NULL;
	float G_B30_0 = 0.0f;
	{
		float L_0 = Player_get_blink_m816255482(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_003d;
		}
	}
	{
		Water_t1083516957 * L_1 = __this->get_pool_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (!L_2)
		{
			G_B3_0 = __this;
			goto IL_002d;
		}
	}
	{
		float L_3 = __this->get_restingStressVelocity_21();
		G_B4_0 = L_3;
		G_B4_1 = G_B2_0;
		goto IL_0033;
	}

IL_002d:
	{
		float L_4 = __this->get_waterStressVelocity_23();
		G_B4_0 = L_4;
		G_B4_1 = G_B3_0;
	}

IL_0033:
	{
		G_B4_1->set_stressVelocity_7(G_B4_0);
		goto IL_00bc;
	}

IL_003d:
	{
		float L_5 = __this->get_blinkTime_9();
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			goto IL_0060;
		}
	}
	{
		float L_6 = __this->get_stressVelocity_7();
		float L_7 = __this->get_blinkPenalty_20();
		__this->set_stressVelocity_7(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
	}

IL_0060:
	{
		float L_8 = __this->get_stressVelocity_7();
		if ((!(((float)L_8) > ((float)(0.0f)))))
		{
			goto IL_00a3;
		}
	}
	{
		float L_9 = __this->get_stressVelocity_7();
		float L_10 = __this->get_stressVelocity_7();
		float L_11 = __this->get_graceTimer_10();
		float L_12 = __this->get_gracePeriod_24();
		__this->set_stressVelocity_7(((float)il2cpp_codegen_subtract((float)L_9, (float)((float)il2cpp_codegen_multiply((float)L_10, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)((float)L_11/(float)L_12)))))))));
		float L_13 = __this->get_gracePeriod_24();
		__this->set_graceTimer_10(L_13);
	}

IL_00a3:
	{
		float L_14 = __this->get_stressVelocity_7();
		float L_15 = __this->get_recoverDelta_19();
		float L_16 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_stressVelocity_7(((float)il2cpp_codegen_subtract((float)L_14, (float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_16)))));
	}

IL_00bc:
	{
		float L_17 = __this->get_graceTimer_10();
		if ((!(((float)L_17) > ((float)(0.0f)))))
		{
			goto IL_00de;
		}
	}
	{
		float L_18 = __this->get_graceTimer_10();
		float L_19 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_graceTimer_10(((float)il2cpp_codegen_subtract((float)L_18, (float)L_19)));
	}

IL_00de:
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (GameObject_t1113636619 *)NULL;
		List_1_t2585711361 * L_20 = __this->get_monsters_13();
		Enumerator_t179987942  L_21 = List_1_GetEnumerator_m1750140655(L_20, /*hidden argument*/List_1_GetEnumerator_m1750140655_RuntimeMethod_var);
		V_3 = L_21;
	}

IL_00f2:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0133;
		}

IL_00f7:
		{
			GameObject_t1113636619 * L_22 = Enumerator_get_Current_m4179928398((Enumerator_t179987942 *)(&V_3), /*hidden argument*/Enumerator_get_Current_m4179928398_RuntimeMethod_var);
			V_2 = L_22;
			Transform_t3600365921 * L_23 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
			Vector3_t3722313464  L_24 = Transform_get_position_m36019626(L_23, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
			Vector2_t2156229523  L_25 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
			GameObject_t1113636619 * L_26 = V_2;
			Transform_t3600365921 * L_27 = GameObject_get_transform_m1369836730(L_26, /*hidden argument*/NULL);
			Vector3_t3722313464  L_28 = Transform_get_position_m36019626(L_27, /*hidden argument*/NULL);
			Vector2_t2156229523  L_29 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
			float L_30 = Vector2_Distance_m3048868881(NULL /*static, unused*/, L_25, L_29, /*hidden argument*/NULL);
			V_4 = L_30;
			float L_31 = V_4;
			float L_32 = V_0;
			if ((!(((float)L_31) < ((float)L_32))))
			{
				goto IL_0133;
			}
		}

IL_012e:
		{
			GameObject_t1113636619 * L_33 = V_2;
			V_1 = L_33;
			float L_34 = V_4;
			V_0 = L_34;
		}

IL_0133:
		{
			bool L_35 = Enumerator_MoveNext_m4286844348((Enumerator_t179987942 *)(&V_3), /*hidden argument*/Enumerator_MoveNext_m4286844348_RuntimeMethod_var);
			if (L_35)
			{
				goto IL_00f7;
			}
		}

IL_013f:
		{
			IL2CPP_LEAVE(0x152, FINALLY_0144);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0144;
	}

FINALLY_0144:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1341201278((Enumerator_t179987942 *)(&V_3), /*hidden argument*/Enumerator_Dispose_m1341201278_RuntimeMethod_var);
		IL2CPP_END_FINALLY(324)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(324)
	{
		IL2CPP_JUMP_TBL(0x152, IL_0152)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0152:
	{
		float L_36 = Player_get_blink_m816255482(__this, /*hidden argument*/NULL);
		if ((!(((float)L_36) == ((float)(0.0f)))))
		{
			goto IL_0199;
		}
	}
	{
		GameObject_t1113636619 * L_37 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_38 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_37, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0199;
		}
	}
	{
		Water_t1083516957 * L_39 = __this->get_pool_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_40 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_39, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0199;
		}
	}
	{
		float L_41 = __this->get_stressVelocity_7();
		float L_42 = V_0;
		__this->set_stressVelocity_7(((float)il2cpp_codegen_add((float)L_41, (float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)((float)((float)L_42/(float)(15.0f))))))));
	}

IL_0199:
	{
		GameObject_t1113636619 * L_43 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_43, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_01cc;
		}
	}
	{
		float L_45 = V_0;
		if ((!(((float)L_45) < ((float)(1.8f)))))
		{
			goto IL_01cc;
		}
	}
	{
		Water_t1083516957 * L_46 = __this->get_pool_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_47 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_46, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01cc;
		}
	}
	{
		__this->set_stressVelocity_7((0.9f));
	}

IL_01cc:
	{
		Water_t1083516957 * L_48 = __this->get_pool_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_48, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01e7;
		}
	}
	{
		G_B30_0 = (0.0f);
		goto IL_01f3;
	}

IL_01e7:
	{
		float L_50 = __this->get_waterBonus_22();
		G_B30_0 = ((float)il2cpp_codegen_subtract((float)(0.0f), (float)L_50));
	}

IL_01f3:
	{
		V_5 = G_B30_0;
		V_6 = (1.0f);
		float L_51 = __this->get_stressVelocity_7();
		float L_52 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = ((float)il2cpp_codegen_multiply((float)L_51, (float)L_52));
		float L_53 = V_7;
		float L_54 = V_6;
		float L_55 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_56 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_53, ((float)il2cpp_codegen_subtract((float)L_54, (float)L_55)), /*hidden argument*/NULL);
		float L_57 = V_5;
		float L_58 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		float L_59 = Mathf_Min_m1073399594(NULL /*static, unused*/, ((float)il2cpp_codegen_subtract((float)L_57, (float)L_58)), (0.0f), /*hidden argument*/NULL);
		float L_60 = Mathf_Max_m3146388979(NULL /*static, unused*/, L_56, L_59, /*hidden argument*/NULL);
		V_7 = L_60;
		float L_61 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		float L_62 = V_7;
		Player_set_stress_m4170066995(__this, ((float)il2cpp_codegen_add((float)L_61, (float)L_62)), /*hidden argument*/NULL);
		float L_63 = __this->get_blinkTime_9();
		float L_64 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_blinkTime_9(((float)il2cpp_codegen_add((float)L_63, (float)L_64)));
		Water_t1083516957 * L_65 = __this->get_pool_12();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_66 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_65, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_028e;
		}
	}
	{
		float L_67 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		float L_68 = V_5;
		if ((!(((float)L_67) == ((float)L_68))))
		{
			goto IL_028e;
		}
	}
	{
		float L_69 = Player_get_blink_m816255482(__this, /*hidden argument*/NULL);
		if ((!(((float)L_69) == ((float)(1.0f)))))
		{
			goto IL_028e;
		}
	}
	{
		Water_t1083516957 * L_70 = __this->get_pool_12();
		Water_Activate_m1474361647(L_70, /*hidden argument*/NULL);
	}

IL_028e:
	{
		return;
	}
}
// System.Void Player::PlayerEffects()
extern "C" IL2CPP_METHOD_ATTR void Player_PlayerEffects_m1429873540 (Player_t3266647312 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_PlayerEffects_m1429873540_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_minHeartrate_16();
		float L_1 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		float L_2 = __this->get_minHeartrate_16();
		float L_3 = __this->get_maxHeartrate_17();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_0, (float)((float)il2cpp_codegen_multiply((float)L_1, (float)((float)il2cpp_codegen_subtract((float)L_2, (float)L_3))))));
		float L_4 = __this->get_heartbeatCooldown_15();
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Min_m1073399594(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		__this->set_heartbeatCooldown_15(L_6);
		float L_7 = __this->get_heartbeatCooldown_15();
		float L_8 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_heartbeatCooldown_15(((float)il2cpp_codegen_subtract((float)L_7, (float)L_8)));
		float L_9 = __this->get_heartbeatCooldown_15();
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_0078;
		}
	}
	{
		AudioSource_t3935305588 * L_10 = __this->get_heartbeat_14();
		float L_11 = Player_get_stress_m4229801089(__this, /*hidden argument*/NULL);
		AudioSource_set_volume_m1273312851(L_10, L_11, /*hidden argument*/NULL);
		AudioSource_t3935305588 * L_12 = __this->get_heartbeat_14();
		AudioSource_Play_m48294159(L_12, /*hidden argument*/NULL);
		float L_13 = __this->get_minHeartrate_16();
		__this->set_heartbeatCooldown_15(L_13);
	}

IL_0078:
	{
		return;
	}
}
// System.Void Player::<Init>m__0(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Player_U3CInitU3Em__0_m3753943790 (Player_t3266647312 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_U3CInitU3Em__0_m3753943790_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = V_0;
		String_t* L_3 = GameObject_get_tag_m3951609671(L_2, /*hidden argument*/NULL);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral760905195, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		List_1_t2585711361 * L_5 = __this->get_monsters_13();
		GameObject_t1113636619 * L_6 = V_0;
		List_1_Add_m2765963565(L_5, L_6, /*hidden argument*/List_1_Add_m2765963565_RuntimeMethod_var);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Player::<Init>m__1(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Player_U3CInitU3Em__1_m102196894 (Player_t3266647312 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_U3CInitU3Em__1_m102196894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m442555142(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GameObject_t1113636619 * L_2 = V_0;
		String_t* L_3 = GameObject_get_tag_m3951609671(L_2, /*hidden argument*/NULL);
		bool L_4 = String_op_Equality_m920492651(NULL /*static, unused*/, L_3, _stringLiteral760905195, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		List_1_t2585711361 * L_5 = __this->get_monsters_13();
		GameObject_t1113636619 * L_6 = V_0;
		List_1_Remove_m4063777476(L_5, L_6, /*hidden argument*/List_1_Remove_m4063777476_RuntimeMethod_var);
	}

IL_0029:
	{
		return;
	}
}
// System.Void Player::<Init>m__2(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Player_U3CInitU3Em__2_m1137546408 (Player_t3266647312 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_U3CInitU3Em__2_m1137546408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Transform_t3600365921 * G_B2_0 = NULL;
	Transform_t3600365921 * G_B1_0 = NULL;
	GameObject_t1113636619 * G_B3_0 = NULL;
	String_t* G_B6_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Transform_get_parent_m835071599(L_1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0015;
		}
	}
	{
		G_B3_0 = ((GameObject_t1113636619 *)(NULL));
		goto IL_001a;
	}

IL_0015:
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(G_B2_0, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		GameObject_t1113636619 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		G_B6_0 = ((String_t*)(NULL));
		goto IL_002a;
	}

IL_0024:
	{
		GameObject_t1113636619 * L_6 = V_0;
		String_t* L_7 = GameObject_get_tag_m3951609671(L_6, /*hidden argument*/NULL);
		G_B6_0 = L_7;
	}

IL_002a:
	{
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, G_B6_0, _stringLiteral78692563, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_006e;
		}
	}
	{
		GameObject_t1113636619 * L_9 = V_0;
		Water_t1083516957 * L_10 = GameObject_GetComponent_TisWater_t1083516957_m400380985(L_9, /*hidden argument*/GameObject_GetComponent_TisWater_t1083516957_m400380985_RuntimeMethod_var);
		__this->set_pool_12(L_10);
		Water_t1083516957 * L_11 = __this->get_pool_12();
		bool L_12 = Water_get_Discovered_m4177957938(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_006e;
		}
	}
	{
		Water_t1083516957 * L_13 = __this->get_pool_12();
		Water_Discover_m2700990817(L_13, /*hidden argument*/NULL);
		int32_t L_14 = Player_get_poolsDiscovered_m1228037983(__this, /*hidden argument*/NULL);
		Player_set_poolsDiscovered_m3631500643(__this, ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1)), /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void Player::<Init>m__3(UnityEngine.Collider2D)
extern "C" IL2CPP_METHOD_ATTR void Player_U3CInitU3Em__3_m2803041481 (Player_t3266647312 * __this, Collider2D_t2806799626 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Player_U3CInitU3Em__3_m2803041481_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	Transform_t3600365921 * G_B2_0 = NULL;
	Transform_t3600365921 * G_B1_0 = NULL;
	GameObject_t1113636619 * G_B3_0 = NULL;
	String_t* G_B6_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = ___collider0;
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(L_0, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Transform_get_parent_m835071599(L_1, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = L_2;
		G_B1_0 = L_3;
		if (L_3)
		{
			G_B2_0 = L_3;
			goto IL_0015;
		}
	}
	{
		G_B3_0 = ((GameObject_t1113636619 *)(NULL));
		goto IL_001a;
	}

IL_0015:
	{
		GameObject_t1113636619 * L_4 = Component_get_gameObject_m442555142(G_B2_0, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		GameObject_t1113636619 * L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		G_B6_0 = ((String_t*)(NULL));
		goto IL_002a;
	}

IL_0024:
	{
		GameObject_t1113636619 * L_6 = V_0;
		String_t* L_7 = GameObject_get_tag_m3951609671(L_6, /*hidden argument*/NULL);
		G_B6_0 = L_7;
	}

IL_002a:
	{
		bool L_8 = String_op_Equality_m920492651(NULL /*static, unused*/, G_B6_0, _stringLiteral78692563, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0040;
		}
	}
	{
		__this->set_pool_12((Water_t1083516957 *)NULL);
	}

IL_0040:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerCamera::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PlayerCamera__ctor_m2292697031 (PlayerCamera_t1622178205 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerCamera::Start()
extern "C" IL2CPP_METHOD_ATTR void PlayerCamera_Start_m3648269509 (PlayerCamera_t1622178205 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerCamera_Start_m3648269509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		Player_t3266647312 * L_1 = GameObject_GetComponent_TisPlayer_t3266647312_m4068145281(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayer_t3266647312_m4068145281_RuntimeMethod_var);
		__this->set_player_4(L_1);
		return;
	}
}
// System.Void PlayerCamera::Update()
extern "C" IL2CPP_METHOD_ATTR void PlayerCamera_Update_m1529155267 (PlayerCamera_t1622178205 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerCamera_Update_m1529155267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	double V_1 = 0.0;
	double V_2 = 0.0;
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Player_t3266647312 * L_0 = __this->get_player_4();
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Transform_get_position_m36019626(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = bankers_roundf(((float)il2cpp_codegen_multiply((float)L_3, (float)(127.0f))));
		V_1 = (((double)((double)((float)((float)L_4/(float)(127.0f))))));
		float L_5 = (&V_0)->get_y_3();
		float L_6 = bankers_roundf(((float)il2cpp_codegen_multiply((float)L_5, (float)(127.0f))));
		V_2 = (((double)((double)((float)((float)L_6/(float)(127.0f))))));
		Transform_t3600365921 * L_7 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		double L_8 = V_1;
		double L_9 = V_2;
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_11 = Transform_get_position_m36019626(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_z_4();
		Vector3_t3722313464  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m3353183577((&L_13), (((float)((float)L_8))), (((float)((float)L_9))), L_12, /*hidden argument*/NULL);
		Transform_set_position_m3387557959(L_7, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerCamera::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C" IL2CPP_METHOD_ATTR void PlayerCamera_OnRenderImage_m341268003 (PlayerCamera_t1622178205 * __this, RenderTexture_t2108887433 * ___source0, RenderTexture_t2108887433 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerCamera_OnRenderImage_m341268003_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Material_t340375123 * L_0 = __this->get_cameraMaterial_5();
		Player_t3266647312 * L_1 = __this->get_player_4();
		float L_2 = Player_get_blink_m816255482(L_1, /*hidden argument*/NULL);
		Material_SetFloat_m3226510453(L_0, _stringLiteral2720163479, L_2, /*hidden argument*/NULL);
		Material_t340375123 * L_3 = __this->get_cameraMaterial_5();
		Player_t3266647312 * L_4 = __this->get_player_4();
		float L_5 = Player_get_stress_m4229801089(L_4, /*hidden argument*/NULL);
		Material_SetFloat_m3226510453(L_3, _stringLiteral1769055757, L_5, /*hidden argument*/NULL);
		RenderTexture_t2108887433 * L_6 = ___source0;
		RenderTexture_t2108887433 * L_7 = ___destination1;
		Material_t340375123 * L_8 = __this->get_cameraMaterial_5();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t783367614_il2cpp_TypeInfo_var);
		Graphics_Blit_m890955694(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Water::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Water__ctor_m1387834787 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Water::get_Active()
extern "C" IL2CPP_METHOD_ATTR bool Water_get_Active_m2068819926 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CActiveU3Ek__BackingField_8();
		return L_0;
	}
}
// System.Void Water::set_Active(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Water_set_Active_m918479718 (Water_t1083516957 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CActiveU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Boolean Water::get_Discovered()
extern "C" IL2CPP_METHOD_ATTR bool Water_get_Discovered_m4177957938 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_U3CDiscoveredU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void Water::set_Discovered(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Water_set_Discovered_m187261137 (Water_t1083516957 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CDiscoveredU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void Water::Awake()
extern "C" IL2CPP_METHOD_ATTR void Water_Awake_m2670714267 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		Water_set_Discovered_m187261137(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::Start()
extern "C" IL2CPP_METHOD_ATTR void Water_Start_m3523044285 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Water_Start_m3523044285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = GameObject_Find_m2032535176(NULL /*static, unused*/, _stringLiteral2261822918, /*hidden argument*/NULL);
		__this->set_player_4(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_2 = Transform_Find_m1729760951(L_1, _stringLiteral2359378951, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m442555142(L_2, /*hidden argument*/NULL);
		__this->set_collision_5(L_3);
		Transform_t3600365921 * L_4 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		Transform_t3600365921 * L_5 = Transform_Find_m1729760951(L_4, _stringLiteral2604872252, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m442555142(L_5, /*hidden argument*/NULL);
		__this->set_trigger_6(L_6);
		GameObject_t1113636619 * L_7 = __this->get_collision_5();
		BoxCollider2D_t3581341831 * L_8 = GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(L_7, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var);
		GameObject_t1113636619 * L_9 = __this->get_player_4();
		BoxCollider2D_t3581341831 * L_10 = GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742(L_9, /*hidden argument*/GameObject_GetComponent_TisBoxCollider2D_t3581341831_m3822577742_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_IgnoreCollision_m4213295628(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::SetActivator(Water/Activator)
extern "C" IL2CPP_METHOD_ATTR void Water_SetActivator_m3781101325 (Water_t1083516957 * __this, Activator_t2573376075 * ___activator0, const RuntimeMethod* method)
{
	{
		Activator_t2573376075 * L_0 = ___activator0;
		__this->set_activator_7(L_0);
		Water_set_Active_m918479718(__this, (bool)1, /*hidden argument*/NULL);
		Water_set_Discovered_m187261137(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::Discover()
extern "C" IL2CPP_METHOD_ATTR void Water_Discover_m2700990817 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		Water_set_Discovered_m187261137(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Water::Activate()
extern "C" IL2CPP_METHOD_ATTR void Water_Activate_m1474361647 (Water_t1083516957 * __this, const RuntimeMethod* method)
{
	{
		Activator_t2573376075 * L_0 = __this->get_activator_7();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Activator_t2573376075 * L_1 = __this->get_activator_7();
		Activator_Invoke_m60796017(L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_Activator_t2573376075 (Activator_t2573376075 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Water/Activator::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Activator__ctor_m1355085645 (Activator_t2573376075 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Water/Activator::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Activator_Invoke_m60796017 (Activator_t2573376075 * __this, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Water/Activator::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Activator_BeginInvoke_m2833081495 (Activator_t2573376075 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Water/Activator::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void Activator_EndInvoke_m3521774661 (Activator_t2573376075 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
